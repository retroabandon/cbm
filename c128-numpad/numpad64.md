Numeric Keypad in 64 Mode
=========================

This program from _COMPUTE!_ 77 (Oct. 1986) p.10, ["Numeric Keypad In 64
Mode"][c77p10] is apparently originally from _COMPUTE!'s 128 Programmer's
Guide._

The BASIC listing is provided in ASCII in [`numpad64.baa`](./numpad64.baa)
and a (hand-created) disassembly in [`numpad64.dis`](./numpad64.dis).



<!-------------------------------------------------------------------->
[c77p10]: https://www.atarimagazines.com/compute/issue77/Feedback_Numeric_Keypad_In_64_Mode.php
