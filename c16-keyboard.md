Commodore 16 Keyboard Hardware/Firmware
=======================================

Contents:
- Interface
- Hardware
  - Key Matrix
  - 6529 Single Port Interface (U13)
  - TED (U1) Keyboard Latch
- ROM Keyboard Routines
  - Keyboard Buffer Read
  - SCNKEY: Keyboard Scan
  - Keyboard Scan Code Decoding Tables
- References


Interface
---------

Keystrokes may be unmodified or modified by just one of `SHIFT`, `C=`
(Commodore key) and `CTRL`. `CTRL` appears to take precedence over `C=`.
- The one instance where two modifiers may be used together is `SHIFT+C=`
  which immediately switches between the "unshifted" graphics character set
  and the "shifted" text character set.
- `C=` when held alone slows the screen scroll speed.
- `CTRL` is used only to access colors, RVS ON/OFF, FLASH ON/OFF and, with
  `CTRL+S`, pause scrolling of the screen.

Modifier keys have different behaviour for the letter rows and number row.
(Function keys are modified by SHIFT only.)

       mod │          alpha rows             │    number row
       key │    graphics        text         │
    ───────┼─────────────────────────────────┼──────────────────────
      None │ Ktop UC          Ktop LC        │ Ktop symbol (upper)
     SHIFT │ Kfront right GR  Ktop UC        │ Ktop number (lower)
    CBM/C= │ Kfront left GR   Kfront left GR │ Kfront lower colour
      CTRL │ (nothing)        (nothing)      │ Kfront upper colour
    ───────┴─────────────────────────────────┴──────────────────────
    Ktop: keytop            UC: upper case
    Kfront: key front       LC: lower case      GR: graphic symbol


Hardware
--------

### Key Matrix

The [keyboard matrix in the service manual][sm-km] has correct keyboard
connector `CN2` pin numbers, but the order in which they are printed on the
page does not match the order of the pins for the 6529 and TED keyboard
port. Columns 3 (TED `K3`, CN2 17) and 7 (TED `K7`, CN2 3) are swapped, as
are rows 0 (6529 `P0`, CN2 19) and 7 (6529 `P7`, CN2 6). The correct
ordering is as follows (`CN2` = keyboard connector):

                  │  K7   K6   K5   K4   K3   K2   K1   K0  ── TED pin
    ┌6529┐    CN2 │   3   10    9    7   17   14   15   18  ── CN2 pin
    │    │ pin │  ├───────────────────────────────────────────────────
    P7   9     6  │ STOP  Q   CBM        2"  CTRL HOME  1!
    P6   8    16  │  /?   +    =   ESC   →    ;]   *    ←
    P5   7     1  │  ,<   -    :[   .>   ↑    L    P    ↓
    P4   6    13  │  N    O    K    M    0^   J    I    9)
    P3   5    11  │  V    U    H    B    8(   G    Y    7'
    P2   4    12  │  X    T    F    C    6&   D    R    5%
    P1   3     8  │ SHFT  E    S    Z    4$   A    W    3#
    P0   2    19  │  @    F3   F2   F1  HELP  £   RETN DEL

Though the keys are the same as the C64, the matrix is different; as well
as different assignments of existing keys, both shift keys on the C16 are
wired to one matrix position in order to free up a matrix point for what
used to be the RESTORE key (which is wired outside the matrix on the C64).

### 6529 Single Port Interface (U13)

The [6529] is directly above the left side of the keyboard connector.

It's similar to an [SN74LS639] except that `P0`-`P7` lines are not
tri-state I/O pins but instead open-collector with internal pull-ups
(measured as around 7-9 kΩ). Thus commanding them high will result in high
output from the pull-ups unless something else on the line is pulling them
low.

XXX Unlike the '639, there must be a latch in the 6529 (unmentioned in the
datasheet), too, otherwise how would a `Pn` line stay low after it's been
written, rather than rising again immediately? It can't be just bus
capacitance. (And further XXX: the Wikipedia article needs to be fixed to
note this.)

Keyboard rows here are taken from [[sm]] and [[sams]] schematics.

    Pins  Function  Notes
    ────────────────────────────────────────────────────────────────
     1      R/W     R/W from CPU
     2-9    P0-P7   keyboard rows: 19 8 12 11 13 1 16 6
                     ([sm] diagram: 6 8 12 11 13 1 16 19)
    10      Vss     GND
    11-18   D7-D0   D7-D0 from/to CPU
    19      C̅S̅      "KEY PORT CS"
    20      Vdd     +5V

### TED (U1) Keyboard Latch

Write any data to [TED] register 8  to read input lines and latch the data.
Read register 8 to read latched data. Pins are 15-22: `K0`-`K7` on [[sm]]
and [[sams]] schematics or `KYBD0`-`KYBD7` in handwritten annotations on
[preliminary data sheet][ted]. Keyboard columns here are taken from
[[sm]] and [[sams]] schematics.

    Joystick connector              ₁1 ₁2 ₁3 ₁4       ₁6 ₂6
    TED pin number                  15 16 17 18 19 20 21 22
    TED pin name                    K0 K1 K2 K3 K4 K5 K6 K7
    TED to KB connector (columns)   18 15 14 17  7  9 10  3
    ───────────────────────────────────────────────────────
    (Keyboard Matrix Diagram)       18 15 14  3  7  9 10 17

XXX What is the purpose of the 2nd pullup after each diode on the joystick
ports? (The diode is biased so that the joystick port can only pull the
line low.) Perhaps related to the 74'125 outputs connected to pin 8?


ROM Keyboard Routines
---------------------

### Keyboard Buffer Read

A 9-character buffer `kchbuf` at $0527 is used to store PETSCII codes
(printable characters and C16 control characters) generated from the
keyboard. The following routine reads a character from the buffer. On
return the carry flag is always clear and the keystroke is returned in A.

Note that function key inputs are not stored in this buffer; they have a
separate system and storage for generating the definitions that have been
assigned to each function key. Unlike the C64, function keys no longer
generate the $85-$8B character codes, though they can be redefined to
generate them. (The codes will still not appear in `kchbuf`, however.)

XXX Determine what happens if kchbuf is empty. Returns A=0, like $FFE4
GETIN? Find the routine that calls this one.

                  kchcnt  EQU $EF       count of chars in kchbuf
                  kchbuf  EQU $0527     keyboard character buffer (9 bytes)
                  kchmax  EQU $053F     holds max length of kchbuf above

    ; Input from keyboard (Gerrad p.125)
    ; This  particular routine does not handle expansion of fkey definitions.
    D8D4 AC 27 05         LDY kchbuf      next char from keyboard input queue
    DBD7 A2 00            LDX #0          index
    D8D9 BD 28 05 .next   LDA kchbuf+1,X  shift remaining chars in the buffer
    D8DC 9D 27 05         STA kchbuf,X      to the left
    D8DF E8               INX
    D8E0 E4 EF            CPX kchcnt      at end of buffer?
    D8E2 D0 F5            BNE .next
    D8E4 C6 EF            DEC kchcnt      one less char in kchbuf
    D8E6 98               TYA             return value to A
    D8E7 58               CLI
    D8E8 18               CLC             clear error flag
    D8E9 60               RTS

### SCNKEY: Keyboard Scan

The buffer is filled by the KERNAL `SCNKEY` routine, officially at $FF9F,
but which is just a jump to the actual code at $DB11. It's normally called
by the IRQ handler ($CE0E).

                  ktabptr EQU $EC         pointer to keyboard decode table in
                                            temp space used by screen routines
                  tmp2    EQU $EE
                  kt_idx  EQU $C6         index of keystroke in ktab
                                            (keyboard decode table)
                  keyrept EQU $0541       key repeat timer counters (2 bytes)
                  kbmod   EQU $0543       mod keys seen during this scan
                                             (SHFT=$01 CBM=$02 CTRL=$04)
                  sc_dly  EQU $0544       delay until we allow Shift-C= to swap
                                            charset again (to give user time
                                            to lift off the keys)
                  kdecodv EQU $0545       vector for keyboard table decoder
                                          (default: $DB7A kdecode)

    ; Read keyboard (Gerrad p.127)
    DB11 A9 00    kbscan  LDA #0          clear set of
    DB13 8D 43 05         STA kbmod         current mod keys
    DB16 A0 40            LDY #ktabsz-1   final entry in keyboard decode table
    DB18 84 C6            STY kt_idx        is default for when no key pressed
    DB1A 20 70 DB         JSR scanrow     A=0: all rows pulled low
    DB1D AA               TAX
    DB1E E0 FF            CPX #$FF        all columns high?
    DB20 D0 03            BNE .fstrow       no: go find what keys are pressed
    DB22 4C 01 DC         JMP .savchar      yes: no keys pressed
    DB25 A0 00    .fstrow LDY #0           decode table index
    DB27 A9 26            LDA #LSB(ktab)   set up pointer to no-modifier
    DB29 85 EC            STA ktabptr        keyboard decode table
    DB2B A9 E0            LDA #MSB(ktab)
    DB2D 85 ED            STA ktabptr+1
    DB2F A9 FE            LDA #$FE        row mask: bits 7-1 set, bit 0 clear
    DB31 A2 08    .nxtrow LDX #8          8 columns to check in upcoming row
    DB33 48               PHA             save current row mask
                          ;   Row mask on stack from here...
    DB34 68       .scan   PLA             copy current row mask
    DB35 48               PHA
    DB36 20 70 DB         JSR scanrow
    DB39 85 EE            STA tmp2        save column output of current row
    DB3B 68               PLA             copy current row mask
    DB3C 48               PHA
    DB3D 20 70 DB         JSR scanrow     scan row a 2nd time
    DB40 C5 EE            CMP tmp2        same as the first time?
    DB42 D0 F0            BNE .scan         no: spurious value, try again
    DB44 4A       .nxtcol LSR             move row's next column value to carry
    DB45 B0 16            BCS .nokey      if high, this key not pressed
    DB47 48               PHA             save remaining column values
                          ;   Check for mod keys: SHFT=$01, CBM=$02, CTRL=$04
    DB48 B1 EC            LDA (ktabptr),Y decode this row and column
    DB4A C9 05            CMP #$05        ≥5, not a mod key
    DB4C B0 0C            BCS .notmod
    DB4E C9 03            CMP #$03        3 is not a mod key
    DB50 F0 08            BEQ .notmod
    DB52 0D 43 05         ORA kbmod       add the mod key to our set
    DB55 8D 43 05         STA kbmod         of current mod keys
    DB58 10 02            BPL .skip
    DB5A 84 C6    .notmod STY kt_idx      save decode table index of this key
    DB5C 68       .skip   PLA             restore remaining column values
    DB5D C8       .nokey  INY             index to next column in decode table
    DB5E C0 41            CPY #ktabsz     reached end of decode table?
    DB60 B0 08            BCS .done       yes: we're done
    DB62 CA               DEX             another column to check?
    DB63 D0 DF            BNE .nxtcol       yes: go check it
    DB65 38               SEC             1 bit to rotate into row mask
                          ;   ...row mask on stack to here.
    DB66 68               PLA             get row mask
    DB67 2A               ROL             move 0 bit to the left
    DB68 D0 C7            BNE .nxtrow
    DB6A 68       .done   PLA             drop current row mask
    DB6B A5 C6            LDA kt_idx
    DB6D 6C 45 05         JMP (kdecodv)

                  keyport EQU $FD30       MOS 6829 (U13) Single Port Interface
                  tedkey  EQU $FF08       TED keyboard port

    DB70 8D 30 FD scanrow STA keyport     enable row selected in A
    DB73 8D 08 FF         STA tedkey      write anything to latch data on K0-K7
    DB76 AD 08 FF         LDA tedkey      read latched data
    DB79 60               RTS

                  ;   Default routine for kdecodv

                  s_pause EQU $F0         screen I/O paused: $00=go $xx=paused
                  lastkey EQU $07F6       last key that was pressed: index
                                            into ktab decode table
                  TED     EQU $FF00       TED chip registers base address

    DB7A AD 43 05 kdecode LDA kbmod       all mod keys we saw in scan
    DB7D C9 03            CMP #(SHFT|CBM) is it Shift+C=?
    DB7F D0 19            BNE .nscbm        no, process as regular mod keys

                          ;   Shift+C= is not a combination of modifier keys
                          ;   but is considered a keystroke (of sorts) itself,
                          ;   a command to switch character sets.
    DB81 AD 47 05         LDA $0547       is Shift+C= enabled? (00=yes; 80=no)
    DB84 30 34            BMI .lookup       no: lookup key in non-mod table
    DB86 AD 44 05         LDA sc_dly      still delaying before we switch
                                            charset again?
    DB89 D0 2F            BNE .lookup       yes: lookup key in non-mod table
    DB8B AD 13 FF         LDA TED+$13     TED register 19 (charbase etc.)
    DB8E 49 04            EOR #$04        flip bit 2
    DB90 8D 13 FF         STA TED+$13       to change character set
    DB93 A9 08            LDA #$08        4 shifts before we will
    DB95 BD 44 05         STA sc_dly        switch charset again
    DB98 D0 20            BNE .lookup     BRA (LDA #$08 cleared Z flag)

    DB9A 0A       .nscbm  ASL             kbmod*2
    DB9B C9 08            CMP #$08        <8 = SHFT or CBM (not CTRL)
    DB9D 90 10            BCC .settab       yes, go find correct decode table
                          ;   Check for Ctrl-S
    DB9F A9 06            LDA #(2*$3)     offset of 4th ktab addr in ktabs
                                            in case we enter .settab below
    DBA1 AE F7 07         LDX $07F7       Ctrl-S/T: $00=enabled $xx=disabled
    DBA4 D0 09            BNE .settab       disabled: carry on
    DBA6 A6 C6            LDX kt_idx
    DBA8 E0 0D            CPX #$0D        is it 'S' key?
    DBAA D0 03            BNE .settab       no, carry on with decoding
    DBAC 86 F0            STX s_pause       yes, pause screen scrolling
    DBAE 60               RTS

                          ;   Set correct decode table based on mod keys.
                          ;   We enter with kbmod*2 in A, giving us an offset
                          ;   into ktabs, the list of decode table addresses:
                          ;       %0000: no mods table
                          ;       %0010: SHFT key table
                          ;       %0100: CBM key table
                          ;       %0110: CTRL key table (set "manually" above)
    DBAF AA       .settab TAX             kbmod*2
    DBB0 BD 1E E0         LDA ktabs,X     look up LSB of the keyboard decode
    DBB3 85 EC            STA ktabptr       table and copy it to our pointer
    DBB5 BD 1F E0         LDA ktabs+1,X   MSB
    DBB8 85 ED            STA ktabptr+1

    DBBA A4 C6    .lookup LDY kt_idx      get the char code decoded during scan
    DBBC B1 EC            LDA (ktabptr),Y find in selected decode table
    DBBE AA               TAX
    DBBF CC F6 07         CPY lastkey     is it same as last key we saw presed?
    DBC2 F0 07            BEQ .repeat       yes, handle key repeat
    DBC4 A0 10            LDY #$10        reset key repeat timer
    DBC6 8C 42 05         STA keyrept+1
    DBC9 D0 36            BNE .savchar    BRA (didn't take BEQ above)

    DBCB 29 7F    .repeat AND #$7F
    DBCD 2C 40 05         BIT $0540       key repeat flag: $80=all $40=none
                                            $00=space, INS/DEL, cursor keys
    DBD0 30 16            BMI .dorep      bit 7 set: all keys repeat
    DBD2 70 57            BVS .rts        bit 6 set: no keys repeat
    DBD4 C9 7F            CMP #$7F
    DBD6 F0 29            BEQ .savchar
    DBD8 C9 14            CMP #DEL
    DBDA F0 0C            BEQ .dorep
    DBDC C9 20            CMP #' '
    DBDE F0 08            BEQ .dorep
    DBE0 C9 1D            CMP #CRT        cursor right
    DBE2 F0 04            BEQ .dorep
    DBE4 C9 11            CMP #CDN        cursor down
    DBE6 D0 43            BNE .rts
    DBE8 AC 42 05 .dorep  LDY keyrept+1
    DBEB F0 05            BEQ .dorep2     if zero, go do other byte of counter
    DBED CE 42 05         DEC keyrept+1   did we make it zero?
    DBF0 D0 39            BNE .rts          no, we don't try to repeat yet
    DBF2 CE 41 05 .dorep2 DEC keyrept     other byte of counter at zero yet?
    DBF5 D0 34            BNE .rts          no, we don't try to repeat yet
    DBF7 A0 04            LDY #$04        re-initialize
    DBF9 8C 41 05         STY keyrept       repeat counter other byte
    DBFC A4 EF            LDY kchcnt      is kchbuf empty?
    DBFE 88               DEY             if count is 0, Y goes negative
    DBFF 10 21            BPL .rts        repeat only if keys are being consumed

                          ;   Save the character for later reading (or do
                          ;   other appropriate processing if it's special).
    DC01 EA       .savchar NOP
    DC02 EA               NOP
    DC03 4E 44 05         LSR sc_dly      reduce delay until we will allow a
                                            charset switch again
    DC06 A4 C6            LDY $C6         "flag for shift/control key input"
    DC08 8C F6 07         STY lastkey
    DC0B E0 FF            CPX #$FF        came from kbscan w/no keys pressed?
    DC0D F0 1C            BEQ .rts          yes: we're done
    DC0F 8A               TXA
    DC10 A2 00            LDX #$00        resume scrolling
    DC12 86 F0            STX s_pause       in case screen scrolling was paused
    DC14 A2 07            LDX #$07        start at end of table
    DC16 DD 41 DC .fkeyck CMP fkeytab,X   is this code an fkey?
    DC19 F0 11            BEQ fkeyproc      yes, set up to read definition
    DC1B CA               DEX             done with table?
    DC1C 10 F8            BPL .fkeyck       no, continue with next entry
    DC1E A6 EF            LDX kchcnt      current count of chars in kchbuf
    DC20 EC 3F 05         CPX kchmax      kchbuf full?
    DC23 B0 06            BCS .rts          yes: drop the character
    DC25 9D 27 05         STA kchbuf,X   store the char
    DC28 E8               INX               and increment the count
    DC29 86 EF            STX kchcnt        of chars in the buffer
    DC2B 60       .rts    RTS

                          ;   Process function key from scan
                          ;   A contains a function key code.
                          ;   X contains its index into fkeytab.
                          ;   Set up $055D/E for read by BASIC GET etc.
    DC2C BD 5F 05 fkeyproc LDA fkdefln,X  get length of definition
    DC2F 8D 5D 05         STA $055D       no. of chars left in fkey definition
    DC32 A9 00            LDA #$00
    DC34 CA               DEX
    DC35 30 06            BMI .fkdone
    DC37 18               CLC
    DC38 7D 5F 05         ADC fkdefln,X
    DC3B 90 F7            BCC $DC34
    DC3D 8D 5E 05 .fkdone STA $055E      index of cur. char in fkey definition
    DC40 60               RTS

                  fkdefln EQU $055F     8 bytes: fkey definition lengths

                  F1      EQU $85
                  F2      EQU $89
                  F3      EQU $86
                  F4      EQU $8A
                  F5      EQU $87
                  F6      EQU $8B
                  F7      EQU $88
                  HELP    EQU $8C         help key

    DC41 85 89 86 fkeytab DB  F1, F2, F3, F4, F5, F6, F7, HELP
         8A 87 8B
         88 8C

    ; Output to screen (Gerrad p.128, disassembly of DC41-DC48 is incorrect)
    DC49 85 CE            STA $CE         store A as last I/O character
    ...

### Keyboard Scan Code Decoding Tables

    ; Keyboard decoding tables (Gerrad p.132)

                        ;          keyboard decode table addresses
    E01E 26 E0 67 E0    ktabs   DW ktab, kt_shft, kt_cbm, kt_ctrl
    E022 A8 E0 E9 E0
                        ;
                        ;   Keyboard decoding tables map scan codes to
                        ;   PETSCII control characters, printable characters,
                        ;   and special codes for shifts. They are 8 bytes
                        ;   for columns 0..7 in row 0 followed by rows 1..7.
                        ;
                        ktabsz  EQU $41         all tables are 65 bytes long
                        ;
                        SHFT    EQU $01
                        CBM     EQU $02
                        CTRL    EQU $04
                        ;
                        SOFF    EQU $08         shift disable
                        SON     EQU $09         shift enable
                        ;
                        DEL     EQU $14         delete key
                        CR      EQU $0D         return key
                        CDN     EQU $11         cursor down
                        CUP     EQU $91         cursor up
                        CLT     EQU $9D         cursor left
                        CRT     EQU $1D         cursor right
                        ESC     EQU $1B
                        HOME    EQU $13
                        STOP    EQU $03
                        ;
                        ;           unmodified keystrokes
    E026 14 0D 5C 8C    ktab    DB  DEL,  CR, '£', HELP, F1,  F2,  F3,  '@',
    E02A 85 89 86 40
    E02E 33 57 41 34            DB  '3', 'W', 'A', '4', 'Z', 'S', 'E',  SHFT,
    E032 5A 53 45 01
    E036 35 52 44 36            DB  '5', 'Q', 'D', '6', 'C', 'F', 'T', 'X',
    E03A 43 46 54 58
    E03E 37 59 47 38            DB  '7', 'Y', 'G', '8', 'B', 'H', 'U', 'V',
    E042 42 48 55 56
    E046 39 49 4A 30            DB  '9', 'I', 'J', '0', 'M', 'K', 'O', 'N',
    E04A 4D 4B 4F 4E                ;
    E04E 11 50 4C 91            DB  CDN, 'P', 'L', CUP, '.', ':', '-', ',',
    E052 2E 3A 2D 2C
    E056 9D 2A 3B 1D            DB  CLT, '*', ';', CRT, ESC, '=', '+', '/',
    E05A 1B 3D 2B 2F
    E05E 31 13 04 32            DB  '1', HOME,CTRL,'2', ' ', CBM, 'Q', STOP,
    E062 20 02 51 03
    E066 FF                     DB  $FF
                        ;
                        INS     EQU $94
                        LF      EQU $8D
                        ;
                        ;           shifted keystrokes (graphics/upper case)
    E067 94 8D A9 88    kt_shft DB  INS,  LF, $A9,  F7,  F4,  F5, F6, $BA,
    E06B 8A 87 8B BA
    ...
    E0A7 FF                     DB  $FF
                        ;
                        ;           additional colors: TED
                        ORN     EQU $81         orange  1 lower
                        BRN     EQU $95         white   2 lower
                        YGR     EQU $96         yel-grn 3 lower
                        PNK     EQU $97         pink    4 lower
                        BLGR    EQU $98         blu-grn 5 lower
                        LBLU    EQU $99         lt.blu  6 lower
                        DBLU    EQU $9A         dk.blu  7 lower
                        LGRN    EQU $9B         lt.grn  8 lower
                        FON     EQU $82         flashing on
                        FOFF    EQU $84         flashing off
                        ;
                        ;           CBM modifier key
    E0A8 94 8D A8 88    kt_cbm  DB  ...
    E0AC 8A 87
    ...
                        ;           base colors: VIC II
                        BLK     EQU $90         black   1 upper
                        WHT     EQU $05         white   2 upper
                        RED     EQU $1C         red     3 upper
                        CYN     EQU $9F         cyan    4 upper
                        PRP     EQU $9C         purple  5 upper
                        GRN     EQU $1E         green   6 upper
                        BLU     EQU $1F         blue    7 upper
                        YEL     EQU $9E         yellow  8 upper
                        RON     EQU $12         reverse on
                        ROFF    EQU $92         reverse off
                        ;
                        ;           control modifier
                        ;           XXX some of these are multi-mod combos
    E0E9 FF FF 1C FF    kt_ctrl DB  $FF, $FF, RED, $FF, $FF, $FF, $FF, $FF
    E0ED FF FF FF FF
    E0F1 1C 17 01 9F            DB  RED, $17,SHFT, CYN, $1A,HOME,SHFT, $FF
    E0F5 1A 13 05 FF
    E0F9 9C 12 04 1E            DB  PRP, RON,STOP, GRN, $03, $06, DEL, $18
    E0FD 03 06 14 18
    E101 1F 19 07 9E            DB  BLU, $19, $07, YEL, CBM,SOFF, $15, $16
    E105 02 08 15 16
    E109 12 09 0A 92            DB  RON, SON, $0A,ROFF,  CR, $0B, $0F,ROFF
    E10D 0D 0B 0F 0E
    E111 FF 10 0C FF            DB  $FF, $10, $0C, $FF,FOFF, ESC, $FF, FON
    E115 84 1B FF 82
    E119 FF FF 1D FF            DB  $FF, $FF, CRT, $FF, ESC, $06, $FF, $FF,
    E11D 1B 06 FF FF
    E121 90 FF FF 05            DB  BLK, $FF, $FF, WHT, $FF, $FF, $11, $FF,
    E125 FF FF 11 FF
    E129 FF                     DB  $FF,


References
----------

#### Books and Manuals

- [Commodore C-16 Manuals][ar-c16] at archive.org. Includes many Plus/4
  manuals as well.
- \[sm] [_Service Manual: Model C16 Computer_][sm], PN-3140001-03,
  Commodore Business Machines, 1984-10.
- \[sams] [_Sam's Computerfacts: Commodore 16_][sams], Howard W. Sams &
  Co., Inc., 1984-12. Includes schematics (very different layout from
  [[sm]]), expected waveforms and service instructions.
- Peter Gerrad, Kevin Bergin, _The Complete Commodore 16 ROM Disassembly._
  Duckworth, 1985. 196 pp. ISBN 0-7156-2004-5. Found at [rceu]. [(32.5 MB
  PDF)][c16romdis]. Has an extremely minimal disassembly, but good enough
  to find the key input ($D8D4, p.125) and keyboard scan ($DB11 p.127)
  routines. The memory map incorrectly says that $FD10-$FD1F is the 6829
  location. That is actually cassette; "KEYPORT CS" from the PLA is
  $FD30-$FD3F (per [[sm]] schematic).

#### Datasheets

- \[TED] [7360RO (TED datasheet)][TED], Comodore, 1983-04.
- \[6529] [6529 Single Port Interface][6529], Commodore Semiconductor
  Group, 1981-04.
- \[SN74LS639] [Types SN54LS638, SN54LS639, SN74LS638, SN74LS639 Octal Bus
  Transceivers][SN74LS639], _The TTL Data Book Volume 2,_ p. 3-1229, Texas
  Instruments, 1981-01, rev. 1983-12.

#### Web Pages and Forums

- \[wp] [MOS Technology SPI][wp], Wikipedia.
- \[f65o] [MOS 6529 replacement?][f65o], 6502.org Forum, 2014-10.



<!-------------------------------------------------------------------->
[ar-c16]: https://archive.org/details/commodorec16manuals
[c16romdis]: https://www.retro-commodore.eu/download.php?file=Duckworth_The_Complete_Commodore16_ROM_Disassembly.pdf
[rceu]: https://www.retro-commodore.eu/misc-8bit/2/
[sams]: https://archive.org/details/SAMS_Computerfacts_Commodore_C16_1984-12_Howard_W_Sams_Co_CC8/page/n0/mode/1up?view=theater
[sm-km]: https://archive.org/details/Service_Manual_Model_C16_Computer_1984-10_Commodore_PN-314001-03/page/n11/mode/1up?view=theater
[sm]: https://archive.org/details/Service_Manual_Model_C16_Computer_1984-10_Commodore_PN-314001-03/page/n0/mode/1up?view=theater

[6529]: http://archive.6502.org/datasheets/mos_6529_spi.pdf
[SN74LS639]: https://archive.org/details/bitsavers_tidataBookVol2_45945352/page/n1279/mode/1up?view=theater
[TED]: https://www.karlstechnology.com/commodore/TED7360-datasheet.pdf

[f65o]: http://forum.6502.org/viewtopic.php?t=3067
[wp]: https://en.wikipedia.org/wiki/MOS_Technology_SPI
