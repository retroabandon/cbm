Multi-platform Binaries for Commdore Machines
=============================================

Here we briefly investigate some "multi-platform" machine-language programs
for Commodore machines, i.e. ones where the same file can run on VIC-20,
C64, TED, etc. We focus on how they bootstrap themselves.

The `.lst` files were produced from the `.prg` files using the
[`petcat`][petcat] utility from VICE.

Sources:
- `fb.prg`: from [CBM FileBrowser v1.6][cbmfb]. This is not a multiplatform
  binary, but a BASIC program that does machine checks and chooses the
  platform-specific binary to load. Still, the machine-detection technique
  might be interesting.



<!-------------------------------------------------------------------->
[cbmfb]: https://github.com/pi1541/Pi1541/blob/master/CBM-FileBrowser_v1.6/sources/fb.txt
[petcat]: https://vice-emu.sourceforge.io/vice_16.html
