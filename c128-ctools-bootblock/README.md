Commodore 128 CP/M
==================

`cpm.d64` is a C128 CP/M diskette image created by [ctools] with the
command `cformat cpm.d64`. (Using `cformat -2` to create a `.d71` image
produces the exact same image output except with many more all-zero blocks
appended.) When booted this will print `BOOTING ...` on the 40-column
screen for a moment and then clear both screens and print:

    ┌────────────────────────────────────────────────────────────────────┐
    │          BOOTING CP/M PLUS                                         │
    │                                                                    │
    │                                                                    │
    │                                                                    │
    │                                                                    │
    │                                                                    │
    │                                                                    │
    │                                                                    │
    │     NO CPM+.SYS FILE - HIT RETURN TO RETRY                         │
    │               DEL TO ENTER C128 MODE                               │
    │                                                                    │
    │                                                              01 09 │
    └────────────────────────────────────────────────────────────────────┘

This is the boot block for a CP/M diskette without the OS installed. Once
the OS has been installed the last byte of the boot block will be changed
from $00 to $FF, and this will change the behaviour of the C128 Z80 ROM.


Autoboot Sector
---------------

The first sector is a C128 autoboot sector and can be extracted
with `dd` or the `c1541` command supplied with VICE:

    dd if=cpm.d64 of=ctools-bootblock.orig bs=256 count=1
    c1541 cpm.d64 -bread cpm.d64 1 0    # track 1 sector 0 is first sector

The boot sector starts with the `CBM` magic value recognised by the C128
(at power-up or with the `BOOT` command in C128 BASIC) that indicates an
autoboot sector with the following header. [[128boot64]]

    43 42 4D        "CBM" identifier (magic number)
    al ah           Load chained blocks start at this little-endian address
    bb              Load chained blocks in this bank
    nn              Count of chained blocks to load
    cc ... 00       Zero-terminated string to print after BOOTING message
    cc ... 00       Zero-terminated string for PRG file to load (?)
    ...             Entry point of 6502 boot code

(It's not clear how the PRG file field works, and [[128boot64]] doesn't
explain it. Setting it seems to do nothing in images created from
`bootblock.bin` image.)

`bootblock.a65` is a reverse-engineered copy of the boot sector that can be
assembled with [Macroassembler AS] to reproduce the original boot block.
The provided `Makefile` will test this by assembling it to `bootblock.bin`
and comparing it with the original. There is also a `viceboot` target
that will build the boot sector, place it in a test image and run VICE's
`x128` on it so you can experiment with it.



<!-------------------------------------------------------------------->
[128boot64]: https://github.com/rhalkyard/128boot64
[Macroassembler AS]: http://john.ccac.rwth-aachen.de:8000/as/
[ctools]: https://bitbucket.org/csoren/ctools.git
