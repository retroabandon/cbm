retroabandon/cbm-re: Commodore Disassemblies and Reverse-engineering
====================================================================

This repository contains various projects related to disassemblies and
reverse-engineering (RE) of Commodore computers. Each project is in its own
subdirectory.

- `c128-ctools-bootblock`: RE, analysis and disassembly of the bootblock
  for C128 CP/M created by Carsten Sørensen's [ctools] for accessing
  Commodore CP/M disk images.
- `c128-numpad`: A program from _COMPUTE!_ to allow use of the C128 numeric
  keypad in C64 mode.
- `c16-keyboard.md`: RE of the hardware and firmware for the C16 keyboard,
  including the 6529 SPI and TED keyboard latch.
- `dos-1.0/`: Disassembly/reverse engineering of the ROM code from 2020
  and early 3030 drive units.
- `multibin`: Research into machine-language programs that can run
  unchanged on multiple Commodore platforms, such as PET, VIC-20 and C64.


<!-------------------------------------------------------------------->
[ctools]: https://bitbucket.org/csoren/ctools.git
