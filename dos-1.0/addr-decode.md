CBM x040 Address Decoding
=========================

This is not complete; it's just a bit of preliminary work to confirm that
the [memory map][mmap] (by William Levak, from [Zimmers][zmap]) is correct
and get people started if they want to follow the schematic.

The drive has two CPUs, a 6502 and 6504 that run on opposite sides of each
other's ϕ2 cycle.

### `UN1` 6502 CPU (subsystem "I")

Selected by `ϕ2` from `UN1` 6502 CPU
- `UL1` (2332 ROM) $E000-$EFFF
- `UH1` (2332 ROM) $F000-$FFFF
- `UJ1` (2332 ROM) unused

`UN1` (6502 CPU) address line routing:
- `IA15` unconnected; address space $0000-$7FFF mirrors $8000-$FFFF.
- `IA14`-`IA12`: →`C,B,A`/`UB3` decoder (below).
- `IA11`-`IA8`: ROMs.
- `IA7`: ROMs;  (inverted) →○`CS1`/`UC1`; →`CS1`/`UE1`
- `IA6`-`IA0`: all devices' address lines.

`A12`-`A14` (`Addr` below) are inputs `A`-`C` on `UB3` ([74LS42] BCD to
decimal decoder), selecting negative-logic outputs `0`-`7` (`Out` below)
for enable signals (`Sig`, if named) to devices

    IA14-12 Address  Out  Sig  Devices
    000 $0000/$8000   0        UC1 (6532) C̅S̅2̅; UE1 (6532) C̅S̅2̅.
    001 $1000/$9000   1   I̅K̅1̅  }
    010 $2000/$A000   2   I̅K̅2̅  } (RAM control on page 2?)
    011 $3000/$B000   3   I̅K̅3̅  }
    100 $4000/$C000   4   I̅K̅4̅  }
    101 $5000/$D000   5        UJ1 (2332) C̅S̅1̅ (other enable CS2 is ϕ2)
    110 $6000/$E000   6        UL1 (2332) C̅S̅1̅
    111 $7000/$F000   7        UH1 (2332) C̅S̅1̅

ϕ2 is routed to the other enable, `CS2` on `U[HJL]1` ROMs.

### `UH3` 6504 CPU (subsystem "F")

Selected by `ϕ2` from `UH3` 6504 CPU:
- `UM3` (6522 VIA)
- `UK3` (6530 RRIOT)

### Parts Information

6532 RIOT addressing (`R̅S̅`=RAM Select):
- [MOS MCS6530 (Memory, I/O, Timer Array)][mcs6530] (RRIOT) datasheet
- Device select: assert `ϕ2` (39) ∧ `CS1` (38) ∧ `C̅S̅2̅` (37)
- `R̅S̅` asserted: `A0`-`A6` are address within 128×8 bit RAM
- `R̅S̅` deasserted: `A0`-`A4` select I/O port/timer registers

6530 RIOT addressing (`RS0`=ROM Select):
- [R6532 RAM-I/O-Timer (RIOT)][r6532] datasheet
- Device select: assert `ϕ2` ∧ others as programmed; see below.
- Ten address lines `A0`-`A9`.
- `RS0` (4) and `A9`-`A6` (5-8) are mask-programmed to route to a choice of
  internal enable lines, to allow for the engineer to determine the memory
  map for ROM, RAM, PIA and timer.
- `CS1` (18) and `CS2` (19) are optionally not mask-programmed for
  selection; otherwise they are `PB5` and `PB6`.
- `ϕ2` also required asserted for selection.



<!-------------------------------------------------------------------->
[74LS42]: http://www.ti.com/lit/gpn/sn74ls42
[mcs6530]: http://archive.6502.org/datasheets/mos_6530_rriot.pdf
[mmap]: ./memorymap.txt
[r6532]: https://web.archive.org/web/20190926022044/http://www.ionpool.net/arcade/gottlieb/technical/datasheets/R6532_datasheet.pdf
[zmap]: http://www.zimmers.net/anonftp/pub/cbm/schematics/drives/old/4040/memorymap.txt
