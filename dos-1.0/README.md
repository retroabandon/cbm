CBM DOS 1.0 Disassemblies
=========================

Files and Directories:
- `README.md`: This file.
- `addr-decode.md`: System address decoding description.
- `rom/`, `sch/`: ROM images and schematics (see below).
- `Test`: Build/test script (see below).
- `dos1-*.*`: Disassembly files (see below).


Hardware
--------

### ROM Images

A listing of all DOS versions and their corresponding drives can be found
on the Wikipedia [Commodore DOS] page.

This directory contains a disassembly of DOS 1.0. The following ROM images
were obtained from [`firmware/drives/old/4040/`][4040frm] in the CBM
Archives at Zimmers.net, and two different 3040 drives have been confirmed
to have these exact images.

    rom/901468-06.bin   ROM DOS 1 E000-EFFF   (left ROM)
    rom/901468-07.bin   ROM DOS 1 F000-FFFF   (right ROM)
    rom/901466-02.bin   RIOT DOS 1.2          (RIOT ROM)

### Schematics

Schematics (Zimmers.net [`schematics/drives/old/4040/`][4040sch]
- `2040-3040-4040_drive_schematics.pdf`: A German manual, but the included
  schematics and layouts are the same as the English versions in the
  `320806-*.gif` files.

### Components

- [MOS 6530] RRIOT: 1024×8 ROM, 64×8 static RAM, 2-port PIA, interval timer
  with interrupt (1-262,144 clock periods).


Disassembly
-----------

### dis6502 Disassembler

The initial disassembly is done with [Steve Fosdick's version of
`dis6502`][fosdick]. (This is based on [Peter Froehlich's version][phf],
which contains a few more commits since the Fosdick fork.)

Currently you'll need a compiled version of `dis6502` in your path to run
the `Test` script. To build it on Debian you'll need the following packages:
- `build-essential`: for `make`, the C compiler, etc.
- `flex`: for `lex`.
- `docutils-common`: for `rst2man`, called as `rst2man.py` in the Makefile.

Typical tweaks to the dis6502 `Makefile` include:
- Change the `dis6502.1` target to run `rst2man` instead of `rst2man.py`
- Change `INSTDIR` to your preferred $PREFIX.

### Test Script

Running `./Test` will generate new disassembly files `dos1-fic.dis` (for
the file interface controller, running on the 6502) and `dos1-dc.dis` (for
the disk controller, running on the 6504.) These files containing the
addresses and data values at the left and a cross-reference table at the
end of the file. They are currently committed to the repo for easy
reference via web views on GitLab. The predefines files,
`dos1-{fic,dc}.def`, contain the start addresses for tracing and some known
symbol names.

We will later use the `dis6502 -a` option to generate a file suitable for
input to an assembler and further reverse-engineering will happen there.
That should be tested by assembling the binary and comparing it to the
original binaries.


Other Sources
-------------

The [`mist64/cbmsrc`] repository contains DOS 2.0 (I think) source code
under the `DOS_4040/` directory, which is not particularly comprehensible
but may help with technical information, particularly about I/O addresses
and the like. For example, the [`dskint`] file shows how to flash the LEDs.


References
----------

- [Commodore 1541 drive memory map][cbm1541mem]



<!-------------------------------------------------------------------->
[4040frm]: http://www.zimmers.net/anonftp/pub/cbm/firmware/drives/old/4040/
[4040sch]: http://www.zimmers.net/anonftp/pub/cbm/schematics/drives/old/4040/
[Commodore DOS]: https://en.wikipedia.org/wiki/Commodore_4040
[MOS 6530]: https://archive.org/details/mos-6530/

[fosdick]: https://github.com/SteveFosdick/dis6502
[phf]: https://bitbucket.org/phf/dis6502

[`mist64/cbmsrc`]: https://github.com/mist64/cbmsrc
[`dskint`]: https://github.com/mist64/cbmsrc/blob/master/DOS_4040/dskint

[cbm1541mem]: https://sta.c64.org/cbm1541mem.html
