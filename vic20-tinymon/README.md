TINYMON1: A Simple Monitor For The VIC
======================================

Tinymon was first published in the [VIC-20 Update][v20update] column by Jim
Butterfield in _COMPUTE!_ issue 20, January 1982, p. 176. The [article
text][manual] (OCR'd, PDF) is in this repo, but does not include the code
itself, which lives at $400 - $817 (including five trailing $00 bytes in
the listing). That code is a BASIC program (entered on a PET, thus the $400
start address) with a relocating loader and the monitor code itself
appended to it. It's intended to be saved to tape and then loaded on a
VIC-20.

### Commands

The following are the commands to display registers and memory, execute
code, exit to BASIC and load and save binary programs to cassette.
Depositing to memory and registers is done by using the screen editor to
move up to displayed data, changing it, and pressing RETURN.

    .X                          exit to basic
    .R                          display 6502 registers
    .M FFFF TTTT                display memory from FFFF to TTTT
    .G AAAA                     jump to address AAAA
    .L "PPPP"                   load program
    .S "PPPP",01,FFFF,TTTT      save program

The monitor stays loaded in memory even after you exit to BASIC. The
easiest way to re-enter it is to execute a `BRK` instruction: find
any zero byte in memory (i.e., where `PEEK(addr)` returns 0) and
jump to that address with `SYS addr`.

### PRG Version

The [Tinymon v1.0][tv10] download disk image has a `tinymon1.prg` file
which has been copied into this repo. It differs slightly in the first
few bytes, presumably because it's been re-saved from a VIC-20.

The code starts witha typical BASIC prefix to execute the appended
machine-language code:

    100 print"{clr}{down}{down}{rvon}{rght}{rght}{rght} tinymon 
    110 print"{down} jim butterfield"
    120 sys(peek(43)+256*peek(44)+078)

Locations 43 and 44 (word at $002B) are the pointer for the start of BASIC
after the initial $00 byte ($1001); thus the start of the machine-language
code is at $104F in memory on the VIC-20, and $50 from the start of the
`.prg` file. (The code starts with $EA $A5 $2D, or `NOP`, `LDA $A5`.) This
machine-language code for the loader and monitor itself has been extracted
to `tinymon.bin`.

### Reverse-Engineered Code

The relocating loader and the monitor have been extracted under the
[`dis6502`] directory, where that disassembler was used to make initial
disassemblies of the code. The final annotated source generated from this
is in [`monitor.a65`] for the monitor itself and [`relocate.a65`] for the
relocating loader. The `Test` script builds these with the [ASL] assembler
and compares them to the original code.

Note that while you can build `monitor.a65` targeted to any address you
like, there is nothing provided here to generate output that can be used
for the relocating loader.


Theory of Operation
-------------------

The initial relocation code is position-independent, but the monitor itself
is not; addresses in the monitor are adjusted by the relocation code.

The code is prefixed by a BASIC program (shown above) that determines where
the BASIC program was loaded and jumps to `relocate.a65`'s entry point
`sysentry` in the appended machine-language code.

The code at `sysentry` reads from the end of the BASIC TEXT area (which
includes the machine-language code appended to the BASIC program)
downwards, copying that to the top of BASIC memory (`memsiz` pointer at
$37) and moving BASIC's `memsiz` down as it does so. It uses $00 suffix
bytes (because we're copying from end to start) to indicate special
handling (the suffix byte is not copied). If the suffix byte is preceded by
a $00 byte, that is copied verbatim; otherwise the preceding two bytes are
an offset from the end of the code to the end address of the relocated code
is added. (Note that offsets are expected always to be negative.) For the
exact details, see the complete disassembly in [`relocate.a65`].

Once copied, it updates BASIC variables to ensure that BASIC won't stomp on
the copied code, and then jumps to the start of the relocated
machine-language program, [`monitor.a65`].

[`monitor.a65`] starts by setting up the BRK handler that it uses as its
entry point. Hardware interrupts (IRQ) and `BRK` instructions are vectored
to the VIC-20 KERNAL ROM, which determines which of the two caused the
interrupt. If it was an IRQ, the KERNAL handles it, but if it was a `BRK`
instruction it loads the vector in `CBINV` at location $0316 and jumps to
that, which transfers control to the monitor.

The monitor then prints a `.` prompt and awaits a command. The code and
comments in [`monitor.a65`] describe how it executes the various commands.



<!-------------------------------------------------------------------->
[`dis6502`]: ./dis6502/
[`monitor.a65`]: ./monitor.a65
[`relocate.a65`]: ./relocate.a65

[ASL]: http://john.ccac.rwth-aachen.de:8000/as/
[manual]: vic20-tinymon1-manual.pdf
[tv10]: https://commodore.software/downloads/download/865-vic20-machine-language-monitors/15941-tinymon-v1-0
[v20update]: https://archive.org/details/1982-01-compute-magazine/page/176/mode/1up?view=theater
