GLOBAL {
    COMMENTS        0;
    #   Increasing COMMENTS will incrementally add addtional information to
    #   the disassembly. This is useful when working on the .info file, but
    #   it should always be set to 0 before committing the output.
    #     1: Divider line after RTS/JMP (in addition to blank already added).
    #     2: Address on each code line.
    #     3: Hex data on each code line.
    #     4: ASCII data on each code line.

    STARTADDR       $8000;
    PAGELENGTH      0;                  # No paging
    CPU             "6502";
};

####################################################################
#   Conventions:
#
# Since we can't get the disassembler to output `foo-1` addresses in most
# cases, though they're heavily used, we instead create a `foo` label for the
# address in question and a `foo_m1` label to represent `foo-1`.

####################################################################
#   References
#
# Fastload cart schematic:
#   https://rr.pokefinder.org/wiki/Epyx_FastLoad
# Disassembly of first bit of the cartridge init code:
#   https://coil.com/p/segra/Reversing-a-C64-Cartridge/ju9z2Jk9m

####################################################################
#   C64 zero page, ROM routines and other system locations.
#   Sources:
#     http://sta.c64.org/cbm64mem.html

ASMINC { FILE "asminc/c64.inc"; };
ASMINC { FILE "asminc/cbm_kernal.inc"; };

#   We don't label the DDR because this address itself is used only once,
#   but $00,X addressing is used in five other places.
#LABEL { ADDR $00; NAME "PIODDR";
#        COMMENT "6510 PIO port data direction register"; };
LABEL { ADDR $01; NAME "PIO";
        COMMENT "6510 PIO port data register"; };

#   Keyboard input buffer
RANGE { START $277; END $280; TYPE Skip; NAME "KEY_BUF"; };
LABEL { ADDR  $276; NAME "KEY_BUF_m1"; };

####################################################################
#   Cartridge Header

RANGE { START $8000; END $8001; TYPE AddrTable; NAME "coldstartvec"; };
RANGE { START $8002; END $8003; TYPE AddrTable; NAME "warmstartvec"; };
RANGE { START $8004; END $8008; TYPE ByteTable; NAME "cartsignature"; };
RANGE { START $8011; END $8025; TYPE TextTable; NAME "copyright"; };
RANGE { START $8026; END $802F; TYPE TextTable; NAME "fastload_str_m1"; };
#   The $8D at the end of the above is PETSCII LF (linefeed)

RANGE { START $800F; END $8010; TYPE ByteTable; };
LABEL { ADDR $8005; NAME "ramsentinel";
        COMMENT "Used to test whether cart ROM is mapped in or not."; };
LABEL { ADDR $800F; NAME "romcrcL";
        COMMENT "CRC of cartridge from $8011 to $9FFF"; };
LABEL { ADDR $8010; NAME "romcrcH"; };

####################################################################
#   Cartridge Code

LABEL { ADDR $8030; NAME "coldstart";
        COMMENT "Entry point at system startup"; };
LABEL { ADDR $803F; NAME ""; };

RANGE { START $8047; END $8056; TYPE RTSTable; NAME "cs2stack";
        COMMENT "Initial stack set up before coldstart does RTS"; };

LABEL { ADDR $8057; NAME "copycodes";
        COMMENT "RTS from coldstart; copies string and code to RAM"; };
LABEL { ADDR $805B; NAME ""; };
LABEL { ADDR $806A; NAME "breaksig";
        COMMENT "Ensures RAM definitely does not hold cart signature?"; };
#   cs2forever is just a local branch, but because it's branching to itself,
#   cc65 won't generate a local label for the target, instead using the addr.
LABEL { ADDR $8072; NAME "cs2forever"; };
LABEL { ADDR $8074; NAME "crcCheck"; };
LABEL { ADDR $807F; NAME "crcAddByte"; };
LABEL { ADDR $8084; NAME ""; };
LABEL { ADDR $809B; NAME "crcFail"; };
LABEL { ADDR $809F; NAME "forever"; };
LABEL { ADDR $80A2; NAME "crcPass"; };

RANGE { START $80BA; END $80C2; TYPE Code; NAME "sub_C009_code";
        COMMENT "This routine is copied to RAM at $C009"; };
LABEL { ADDR $80B9; NAME "sub_C009_code_m1"; };
LABEL { ADDR $C008; NAME "sub_C009_m1"; };
LABEL { ADDR $C009; NAME "sub_C009"; };

#   This text range may be too broad or narrow; this is just a first stab.
RANGE { START $8DED; END $9004; TYPE TextTable; };

####################################################################
#   Cartridge I/O1 area
#
#   There aren't actually any registers or code here, but an access to any
#   location in the I/O1 area will bring /IO1 low, discharging the C1 capacitor
#   a bit and eventually bringing /EXROM low and enabling cartridge ROM in the
#   $8000-$9FFF range. See the schematic in the references above.

LABEL { ADDR $DE00; NAME "IO1"; };

####################################################################
#   Cartridge code at $9F00-$9FFF is always mapped in at the IO2 area
#   $DF00-$DFFF even when /EXROM is high and $8000-$9FFF is mapped out.

RANGE { START $9F00; END $9FFB; TYPE Code; };
RANGE { START $9FFC; END $9FFF; TYPE ByteTable; };  # Unused

LABEL { ADDR $9F00; NAME "sub_DF00_src"; };

LABEL { ADDR $9F06; NAME "sub_DF06_src";
        COMMENT "???"; };
LABEL { ADDR $DF06; NAME "sub_DF06"; };

LABEL { ADDR $9F15; NAME "pokeIO1_src";
        COMMENT "Mask interrupts and poke I/O1 for a while if carry set"; };
LABEL { ADDR $DF15; NAME "pokeIO1"; };
LABEL { ADDR $9F18; NAME "pokeIO1a"; }; # Really $DF18 after relocation

LABEL { ADDR $9F27; NAME "sub_DF27_src"; };
LABEL { ADDR $DF27; NAME "sub_DF27"; };

#   Not totally sure if comment here is correct.
#   XXX And why LDA $A5 (%10100101)? Bit 5 must be 1 to ensure the cassette
#   motor is off. But bit 7? Maybe because LDA itself is $A5 and this code
#   offset by 1 is also called? But the code doesn't make sense offset by 1...
LABEL { ADDR $9F34; NAME "setHIRAM_src";
        COMMENT "Store %10100101 to PIO; banks out ROML/BASIC/KERNAL?"; };
LABEL { ADDR $DF34; NAME "setHIRAM"; };

LABEL { ADDR $9F38; NAME "delayedcli_src";
        COMMENT "Delay for a while and then enable interrupts"; };
LABEL { ADDR $DF38; NAME "delayedcli"; };

LABEL { ADDR $9F46; NAME "sub_DF46_src"; };
LABEL { ADDR $DF46; NAME "sub_DF46"; };

LABEL { ADDR $9F4D; NAME "sub_DF4D_rts"; };

#   Local branches in this area
LABEL { ADDR $9F1A; NAME ""; };
LABEL { ADDR $9F3C; NAME ""; };
