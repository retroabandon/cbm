        .org $8000
; da65 V2.19 - Git 36132a
; Input file: fastload.bin
; Page:       1


        .setcpu "6502"

PIO             := $0001                        ; 6510 PIO port data register
YSIZE           := $0019
XSIZE           := $0028
VARTAB          := $002D                        ; Pointer to start of BASIC variables
MEMSIZE         := $0037                        ; Pointer to highest BASIC RAM location (+1)
BASIC_BUF_LEN   := $0059                        ; Maximum length of command-line
L0073           := $0073
TXTPTR          := $007A                        ; Pointer into BASIC source code
TIME            := $00A0                        ; 60 HZ clock
FNAM_LEN        := $00B7                        ; Length of filename
SECADR          := $00B9                        ; Secondary address
DEVNUM          := $00BA                        ; Device number
FNAM            := $00BB                        ; Pointer to filename
KEY_COUNT       := $00C6                        ; Number of keys in input buffer
RVS             := $00C7                        ; Reverse flag
CURS_FLAG       := $00CC                        ; 1 = cursor off
CURS_BLINK      := $00CD                        ; Blink counter
CURS_CHAR       := $00CE                        ; Character under the cursor
CURS_STATE      := $00CF                        ; Cursor blink state
SCREEN_PTR      := $00D1                        ; Pointer to current char in text screen
CURS_X          := $00D3                        ; Cursor column
CURS_Y          := $00D6                        ; Cursor row
CRAM_PTR        := $00F3                        ; Pointer to current char in color RAM
FREKZP          := $00FB                        ; Five unused bytes
L0180           := $0180
BASIC_BUF       := $0200                        ; Location of command-line
KEY_BUF_m1      := $0276
KEY_BUF         := $0277
CHARCOLOR       := $0286
CURS_COLOR      := $0287                        ; Color under the cursor
KBDREPEAT       := $028A
KBDREPEATRATE   := $028B
KBDREPEATDELAY  := $028C
PALFLAG         := $02A6                        ; $01 = PAL, $00 = NTSC
L02A7           := $02A7
L02B1           := $02B1
L02DF           := $02DF
IRQVec          := $0314
BRKVec          := $0316
NMIVec          := $0318
L034B           := $034B
L0500           := $0500
L0554           := $0554
L05D7           := $05D7
L0C00           := $0C00
L0D0D           := $0D0D
L2020           := $2020
L2045           := $2045
L3054           := $3054
L3354           := $3354
L3B54           := $3B54
L40A0           := $40A0
L414F           := $414F
L6CCC           := $6CCC
L7300           := $7300
LA490           := $A490
LA560           := $A560
LBDCD           := $BDCD
sub_C009_m1     := $C008
sub_C009        := $C009
LC146           := $C146
LC194           := $C194
LC7F5           := $C7F5
VIC_SPR0_X      := $D000
VIC_SPR0_Y      := $D001
VIC_SPR1_X      := $D002
VIC_SPR1_Y      := $D003
VIC_SPR2_X      := $D004
VIC_SPR2_Y      := $D005
VIC_SPR3_X      := $D006
VIC_SPR3_Y      := $D007
VIC_SPR4_X      := $D008
VIC_SPR4_Y      := $D009
VIC_SPR5_X      := $D00A
VIC_SPR5_Y      := $D00B
VIC_SPR6_X      := $D00C
VIC_SPR6_Y      := $D00D
VIC_SPR7_X      := $D00E
VIC_SPR7_Y      := $D00F
VIC_SPR_HI_X    := $D010
VIC_CTRL1       := $D011
VIC_HLINE       := $D012
VIC_LPEN_X      := $D013
VIC_LPEN_Y      := $D014
VIC_SPR_ENA     := $D015
VIC_CTRL2       := $D016
VIC_SPR_EXP_Y   := $D017
VIC_VIDEO_ADR   := $D018
VIC_IRR         := $D019                        ; Interrupt request register
VIC_IMR         := $D01A                        ; Interrupt mask register
VIC_SPR_BG_PRIO := $D01B
VIC_SPR_MCOLOR  := $D01C
VIC_SPR_EXP_X   := $D01D
VIC_BORDERCOLOR := $D020
VIC_BG_COLOR0   := $D021
VIC_BG_COLOR1   := $D022
VIC_BG_COLOR2   := $D023
VIC_BG_COLOR3   := $D024
VIC_SPR_MCOLOR0 := $D025
VIC_SPR_MCOLOR1 := $D026
VIC_SPR0_COLOR  := $D027
VIC_SPR1_COLOR  := $D028
VIC_SPR2_COLOR  := $D029
VIC_SPR3_COLOR  := $D02A
VIC_SPR4_COLOR  := $D02B
VIC_SPR5_COLOR  := $D02C
VIC_SPR6_COLOR  := $D02D
VIC_SPR7_COLOR  := $D02E
VIC_KBD_128     := $D02F                        ; Extended kbd bits (visible in 64 mode)
VIC_CLK_128     := $D030                        ; Clock rate register (visible in 64 mode)
SCPU_VIC_Bank1  := $D075
SCPU_Slow       := $D07A
SCPU_Fast       := $D07B
SCPU_EnableRegs := $D07E
SCPU_DisableRegs:= $D07F
SCPU_Detect     := $D0BC
SID_S1Lo        := $D400
SID_S1Hi        := $D401
SID_PB1Lo       := $D402
SID_PB1Hi       := $D403
SID_Ctl1        := $D404
SID_AD1         := $D405
SID_SUR1        := $D406
SID_S2Lo        := $D407
SID_S2Hi        := $D408
SID_PB2Lo       := $D409
SID_PB2Hi       := $D40A
SID_Ctl2        := $D40B
SID_AD2         := $D40C
SID_SUR2        := $D40D
SID_S3Lo        := $D40E
SID_S3Hi        := $D40F
SID_PB3Lo       := $D410
SID_PB3Hi       := $D411
SID_Ctl3        := $D412
SID_AD3         := $D413
SID_SUR3        := $D414
SID_FltLo       := $D415
SID_FltHi       := $D416
SID_FltCtl      := $D417
SID_Amp         := $D418
SID_ADConv1     := $D419
SID_ADConv2     := $D41A
SID_Noise       := $D41B
SID_Read3       := $D41C
VDC_INDEX       := $D600
VDC_DATA        := $D601
LDAC0           := $DAC0
CIA1_PRA        := $DC00                        ; Port A
CIA1_PRB        := $DC01                        ; Port B
CIA1_DDRA       := $DC02                        ; Data direction register for port A
CIA1_DDRB       := $DC03                        ; Data direction register for port B
CIA1_TA         := $DC04                        ; 16-bit timer A
CIA1_TB         := $DC06                        ; 16-bit timer B
CIA1_TOD10      := $DC08                        ; Time-of-day tenths of a second
CIA1_TODSEC     := $DC09                        ; Time-of-day seconds
CIA1_TODMIN     := $DC0A                        ; Time-of-day minutes
CIA1_TODHR      := $DC0B                        ; Time-of-day hours
CIA1_SDR        := $DC0C                        ; Serial data register
CIA1_ICR        := $DC0D                        ; Interrupt control register
CIA1_CRA        := $DC0E                        ; Control register for timer A
CIA1_CRB        := $DC0F                        ; Control register for timer B
CIA2_PRA        := $DD00
CIA2_PRB        := $DD01
CIA2_DDRA       := $DD02
CIA2_DDRB       := $DD03
CIA2_TA         := $DD04
CIA2_TB         := $DD06
CIA2_TOD10      := $DD08
CIA2_TODSEC     := $DD09
CIA2_TODMIN     := $DD0A
CIA2_TODHR      := $DD0B
CIA2_SDR        := $DD0C
CIA2_ICR        := $DD0D
CIA2_CRA        := $DD0E
CIA2_CRB        := $DD0F
IO1             := $DE00
LDF00           := $DF00
sub_DF06        := $DF06
LDF14           := $DF14
pokeIO1         := $DF15
sub_DF27        := $DF27
setHIRAM        := $DF34
delayedcli      := $DF38
sub_DF46        := $DF46
LDF4E           := $DF4E
LDF54           := $DF54
LDF5E           := $DF5E
LDF64           := $DF64
LDF70           := $DF70
LDF76           := $DF76
LDF7D           := $DF7D
LDF83           := $DF83
LDF89           := $DF89
LDF8F           := $DF8F
LDF95           := $DF95
LDF9B           := $DF9B
LDFA1           := $DFA1
LDFA5           := $DFA5
LDFAB           := $DFAB
LDFB1           := $DFB1
LDFCF           := $DFCF
LDFDB           := $DFDB
LDFE2           := $DFE2
LDFE8           := $DFE8
LDFF4           := $DFF4
LE171           := $E171
LE453           := $E453
CLRSCR          := $E544
KBDREAD         := $E5B4
LE60A           := $E60A
LE716           := $E716
UPDCRAMPTR      := $EA24
LEB48           := $EB48
LED11           := $ED11
LF299           := $F299
LF3D5           := $F3D5
LF5AF           := $F5AF
LF5D2           := $F5D2
LFE5E           := $FE5E
NMIEXIT         := $FEBC
CINT            := $FF81
IOINIT          := $FF84
RAMTAS          := $FF87
RESTOR          := $FF8A
VECTOR          := $FF8D
SETMSG          := $FF90
SECOND          := $FF93
TKSA            := $FF96
MEMTOP          := $FF99
MEMBOT          := $FF9C
SCNKEY          := $FF9F
SETTMO          := $FFA2
ACPTR           := $FFA5
CIOUT           := $FFA8
UNTLK           := $FFAB
UNLSN           := $FFAE
LISTEN          := $FFB1
TALK            := $FFB4
READST          := $FFB7
SETLFS          := $FFBA
SETNAM          := $FFBD
OPEN            := $FFC0
CLOSE           := $FFC3
CHKIN           := $FFC6
CKOUT           := $FFC9
CLRCH           := $FFCC
BASIN           := $FFCF
BSOUT           := $FFD2
LOAD            := $FFD5
SAVE            := $FFD8
SETTIM          := $FFDB
RDTIM           := $FFDE
STOP            := $FFE1
GETIN           := $FFE4
CLALL           := $FFE7
UDTIM           := $FFEA
SCREEN          := $FFED
PLOT            := $FFF0
IOBASE          := $FFF3
coldstartvec:
        .addr   coldstart
warmstartvec:
        .addr   LFE5E
cartsignature:
        .byte   $C3
; Used to test whether cart ROM is mapped in or not.
ramsentinel:
        .byte   $C2,$CD,$38,$30
L8009:  jsr     L9004
        jmp     delayedcli

; CRC of cartridge from $8011 to $9FFF
romcrcL:.byte   $AB
romcrcH:.byte   $D6
copyright:
        .byte   "(C) 1984 SCOTT NELSON"


fastload_str_m1:
        .byte   $0D
        .byte   "FASTLOAD"
        .byte   $8D
; Entry point at system startup
coldstart:
        sei
        cld
        ldx     #$FF
        txs
        lda     #$27
        sta     PIO
        lda     #$2F
        sta     $00
        ldx     #$0F
:       lda     cs2stack,x
        pha
        dex
        bpl     :-
        rts

; Initial stack set up before coldstart does RTS
cs2stack:
        .word   IOINIT-1
        .word   pokeIO1-1
        .word   L80FA-1
        .word   RESTOR-1
        .word   CINT-1
        .word   pokeIO1-1
        .word   L80C3-1
        .word   copycodes-1
; RTS from coldstart; copies string and code to RAM
copycodes:
        ldx     #$09
        stx     KEY_COUNT
:       lda     fastload_str_m1,x
        sta     KEY_BUF_m1,x
        lda     sub_C009_code_m1,x
        sta     sub_C009_m1,x
        dex
        bne     :-
; Ensures RAM definitely does not hold cart signature?
breaksig:
        lda     #$AA
        sta     ramsentinel
        jsr     sub_C009
cs2forever:
        bne     cs2forever
crcCheck:
        lda     #$00
        tax
        sta     $AE
        lda     #$80
        sta     $AF
        ldy     #$11
crcAddByte:
        adc     ($AE),y
        bcc     :+
        inx
:       iny
        bne     crcAddByte
        inc     $AF
        ldy     $AF
        cpy     #$A0
        ldy     #$00
        bcc     crcAddByte
        cmp     romcrcL
        bne     crcFail
        cpx     romcrcH
        beq     crcPass
crcFail:tay
        jsr     L946B
forever:jmp     forever

crcPass:ldx     #$08
        stx     DEVNUM
        lda     #$27
        sta     $A5
        ldx     #$FB
        txs
        lda     #$34
        pha
        plp
        lda     #$E3
        pha
        lda     #$96
        pha
        .byte   $4C
        .byte   $34
sub_C009_code_m1:
        .byte   $DF
; This routine is copied to RAM at $C009
sub_C009_code:
        jsr     delayedcli
        cmp     ramsentinel
        jmp     pokeIO1

L80C3:  jsr     LDF64
L80C6:  lda     $0331
        cmp     #$F4
        bne     L80D7
        lda     #$DF
        sta     $0331
        lda     #$2E
        sta     $0330
L80D7:  lda     $0303
        cmp     #$A4
        bne     L80E8
        lda     #$6A
        sta     $0302
        lda     #$DF
        sta     $0303
L80E8:  lda     $0290
        cmp     #$EB
        bne     L80F9
        lda     #$DF
        sta     $0290
        lda     #$B7
        sta     $028F
L80F9:  rts

L80FA:  lda     #$00
        tay
L80FD:  sta     BASIC_BUF,y
        .byte   $99
L8101:  brk
        .byte   $03
        sta     $02,y
        iny
        bne     L80FD
        lda     #$3C
        ldx     #$03
        sta     $B2
        stx     $B3
        ldy     #$A0
        sty     $0284
        lda     #$08
        sta     $0282
        lsr     a
        sta     $0288
        rts

L8120:  sta     $93
        lda     DEVNUM
        cmp     #$09
        beq     L812C
        cmp     #$08
        bne     L813F
L812C:  lda     $93
        bne     L813F
        ldy     #$00
        ldx     #$37
        lda     #$35
        jsr     sub_DF27
        ldx     $C3
        cmp     #$24
        bne     L8148
L813F:  lda     #$F4
        pha
        lda     #$A6
        pha
        jmp     setHIRAM

L8148:  lda     SECADR
        beq     L8150
        lda     #$80
        sta     SECADR
L8150:  lda     VIC_IMR
        and     #$0F
        ora     SECADR
        sta     SECADR
        lda     #$00
        sta     VIC_IMR
        jsr     LDFAB
        jsr     L81EE
        bcc     L816B
        lda     #$05
        jmp     L81C0

L816B:  lda     VIC_SPR_ENA
        sta     $A4
        lda     #$00
        sta     VIC_SPR_ENA
        jsr     L9106
        jsr     L827A
        jsr     L8425
        tax
        bne     L8186
        sec
        lda     #$04
        bcs     L81C0
L8186:  pha
        jsr     LDFB1
        pla
        tax
        jsr     L8425
        sta     $AE
        jsr     L8425
        sta     $AF
        dex
        dex
        bit     SECADR
        bmi     L81A4
        lda     $C3
        sta     $AE
        lda     $C4
        sta     $AF
L81A4:  jsr     L8425
        ldy     #$00
        sta     ($AE),y
        inc     $AE
        bne     L81B1
        inc     $AF
L81B1:  dex
        bne     L81A4
        jsr     L8558
        jsr     L8425
        tax
        bne     L81A4
        lda     #$40
        clc
L81C0:  pha
        lda     $A4
        sta     VIC_SPR_ENA
        php
        jsr     L80C6
        jsr     L8572
        lda     $A5
        pha
        lda     #$40
        sta     $A3
        sta     $94
        jsr     LDF5E
        pla
        sta     $A5
        plp
        pla
        sta     $90
        lda     SECADR
        sta     VIC_IMR
        lda     #$60
        sta     SECADR
        ldx     $AE
        jmp     sub_DF46

L81EE:  jsr     L8202
        bcs     L8200
        ldy     #$00
L81F5:  lda     L9124,y
        jsr     L8290
        iny
        bne     L81F5
        clc
        rts

L8200:  sec
        rts

L8202:  ldx     #$00
L8204:  lda     CIA2_PRA
        sta     $90
        asl     a
        asl     a
        eor     $90
        eor     #$C0
        and     #$C0
        beq     L821C
        dey
        bne     L8204
        dex
        bne     L8204
        jmp     L8200

L821C:  lda     $A5
        pha
        jsr     L841C
        sec
        lda     $90
        bne     L826F
        lda     #$E9
        ldx     #$92
        sta     $AE
        stx     $AF
L822F:  jsr     L82CD
        ldy     #$00
L8234:  lda     ($AE),y
        jsr     LDF4E
        iny
        cpy     #$19
        bne     L8234
        lda     #$0D
        jsr     LDF4E
        jsr     LDF5E
        jsr     L841C
        lda     $AE
        clc
        adc     #$19
        sta     $AE
        bcc     L8254
        inc     $AF
L8254:  cmp     #$33
        lda     $AF
        sbc     #$93
        bcc     L822F
        jsr     L840E
        jsr     LDF5E
L8262:  bit     CIA2_PRA
        bvs     L8262
        lda     CIA2_PRA
        ora     #$20
        sta     CIA2_PRA
L826F:  pla
        sta     $A5
        rts

L8273:  lda     DEVNUM
        ora     #$20
        jmp     LDF54

L827A:  lda     FNAM_LEN
        tay
        jmp     L8287

L8280:  lda     #$35
        ldx     #$37
        jsr     sub_DF27
L8287:  jsr     L8290
        dey
        bpl     L8280
        jmp     L8558

L8290:  sta     $90
        jsr     L82A9
        jsr     L82A9
        jsr     L82A9
        jsr     L82A9
        lda     CIA2_PRA
        and     #$0F
        ora     #$20
        sta     CIA2_PRA
        rts

L82A9:  lda     CIA2_PRA
        and     #$0F
        ora     #$10
        lsr     $90
        bcc     L82B6
        ora     #$20
L82B6:  sta     CIA2_PRA
        nop
        nop
        nop
        nop
        nop
        and     #$0F
        lsr     $90
        bcc     L82C6
        ora     #$20
L82C6:  sta     CIA2_PRA
        nop
        nop
        nop
        rts

L82CD:  lda     #$4D
        jsr     LDF4E
        lda     #$2D
        jsr     LDF4E
        lda     #$57
        jsr     LDF4E
        lda     $AE
        sec
        sbc     #$69
        php
        clc
        jsr     LDF4E
        plp
        lda     $AF
        sbc     #$91
        clc
        jsr     LDF4E
        lda     #$19
        jmp     LDF4E

        jsr     LC7F5
        lda     $B7B8
        cmp     #$F5
        beq     L830A
        ldx     #$02
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
L830A:  .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        brk
        brk
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        brk
        brk
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        brk
        brk
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        brk
        brk
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        brk
        brk
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        brk
        brk
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        brk
        brk
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        brk
        brk
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        brk
        brk
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        brk
        brk
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        brk
        brk
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        brk
        brk
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        brk
        brk
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        brk
        brk
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        brk
        brk
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        brk
        brk
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        brk
        brk
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        brk
        brk
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        brk
        brk
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        brk
        brk
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        brk
        brk
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        brk
        brk
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        brk
        brk
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        brk
        brk
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        brk
        brk
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        brk
        brk
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $0F
        brk
        brk
L8408:  eor     $452D
        lda     #$01
        .byte   $0D
L840E:  ldy     #$00
L8410:  lda     L8408,y
        jsr     LDF4E
        iny
        cpy     #$06
        bne     L8410
        rts

L841C:  lda     #$00
        sta     $90
        ldx     #$6F
        jmp     L8273

L8425:  ldy     VIC_HLINE
        lda     ($94),y
        beq     L8425
        and     CIA2_PRA
        sta     CIA2_PRA
        ora     #$20
        tay
        nop
        nop
        nop
        nop
        lda     CIA2_PRA
        lsr     a
        lsr     a
        nop
        eor     CIA2_PRA
        lsr     a
        lsr     a
        nop
        eor     CIA2_PRA
        lsr     a
        lsr     a
        nop
        eor     CIA2_PRA
        sty     CIA2_PRA
        eor     $A3
        tay
        lda     L8458,y
        rts

L8458:  brk
        .byte   $80
        jsr     L40A0
        cpy     #$60
        cpx     #$10
        bcc     L8493
        bcs     L84B5
        bne     L84D7
        beq     L8471
        dey
        plp
        tay
        pha
        iny
        pla
        inx
        clc
L8471:  tya
        sec
        clv
        cli
        cld
        sei
        sed
        .byte   $02
        .byte   $82
        .byte   $22
        ldx     #$42
        .byte   $C2
        .byte   $62
        .byte   $E2
        .byte   $12
        .byte   $92
        .byte   $32
        .byte   $B2
        .byte   $52
        .byte   $D2
        .byte   $72
        .byte   $F2
        asl     a
        txa
        rol     a
        tax
        lsr     a
        dex
        ror     a
        nop
        .byte   $1A
        txs
        .byte   $3A
L8493:  tsx
        .byte   $5A
        .byte   $DA
        .byte   $7A
        .byte   $FA
        .byte   $04
        sty     $24
        ldy     $44
        cpy     $64
        cpx     $14
        sty     $34,x
        ldy     $54,x
        .byte   $D4
        .byte   $74
        .byte   $F4
        .byte   $0C
        sty     $AC2C
        jmp     L6CCC

        cpx     L9C1C
        .byte   $3C
        .byte   $BC
        .byte   $5C
L84B5:  .byte   $DC
        .byte   $7C
        .byte   $FC
        asl     $86
        rol     $A6
        lsr     KEY_COUNT
        ror     $E6
        asl     $96,x
        rol     $B6,x
        lsr     CURS_Y,x
        ror     $F6,x
        asl     $2E8E
        ldx     $CE4E
        ror     $1EEE
        .byte   $9E
        rol     $5EBE,x
        .byte   $DE
        .byte   $7E
L84D7:  inc     L8101,x
        and     ($A1,x)
        eor     ($C1,x)
        adc     ($E1,x)
        ora     ($91),y
        and     ($B1),y
        eor     (SCREEN_PTR),y
        adc     ($F1),y
        ora     #$89
        and     #$A9
        eor     #$C9
        adc     #$E9
        ora     $3999,y
        lda     $D959,y
        adc     $03F9,y
        .byte   $83
        .byte   $23
        .byte   $A3
        .byte   $43
        .byte   $C3
        .byte   $63
        .byte   $E3
        .byte   $13
        .byte   $93
        .byte   $33
        .byte   $B3
        .byte   $53
        .byte   $D3
        .byte   $73
        .byte   $F3
        .byte   $0B
        .byte   $8B
        .byte   $2B
        .byte   $AB
        .byte   $4B
        .byte   $CB
        .byte   $6B
        .byte   $EB
        .byte   $1B
        .byte   $9B
        .byte   $3B
        .byte   $BB
        .byte   $5B
        .byte   $DB
        .byte   $7B
        .byte   $FB
        ora     $85
        and     $A5
        eor     $C5
        adc     $E5
        ora     $95,x
        and     $B5,x
        eor     $D5,x
        adc     $F5,x
        .byte   $0D
L8529:  sta     $AD2D
        eor     $6DCD
        sbc     $9D1D
        and     $5DBD,x
        cmp     $FD7D,x
        .byte   $07
        .byte   $87
        .byte   $27
        .byte   $A7
        .byte   $47
        .byte   $C7
        .byte   $67
        .byte   $E7
        .byte   $17
        .byte   $97
        .byte   $37
        .byte   $B7
        .byte   $57
        .byte   $D7
        .byte   $77
        .byte   $F7
        .byte   $0F
        .byte   $8F
        .byte   $2F
        .byte   $AF
        .byte   $4F
        .byte   $CF
        .byte   $6F
        .byte   $EF
        .byte   $1F
        .byte   $9F
        .byte   $3F
        .byte   $BF
        .byte   $5F
        .byte   $DF
        .byte   $7F
        .byte   $FF
L8558:  lda     CIA2_PRA
        and     #$0F
        ora     #$20
        sta     CIA2_PRA
        jsr     L8571
        lda     #$40
L8567:  bit     CIA2_PRA
        beq     L8567
        lda     #$7F
L856E:  lsr     a
        bcs     L856E
L8571:  rts

L8572:  lda     $028F
        cmp     #$B8
        bne     L8571
        dec     $028F
        lda     $AF
        cmp     #$04
        bne     L8599
        ldx     #$06
L8584:  lda     $03C0,x
        cmp     L8592,x
        bne     L8571
        dex
        bpl     L8584
        .byte   $4C
        .byte   $4D
L8591:  .byte   $86
L8592:  .byte   $CF
        .byte   $FF
        sta     (FREKZP),y
        iny
        bne     L8591
L8599:  lda     #$80
        sta     $94
        lda     #$08
        sta     $95
        ldx     #$40
L85A3:  ldy     #$00
L85A5:  lda     ($94),y
        cmp     L85DB,y
        bne     L85D2
        iny
        cpy     #$04
        bne     L85A5
        ldy     #$02
L85B3:  lda     L85D8,y
        sta     ($94),y
        dey
        bpl     L85B3
        ldx     #$00
L85BD:  lda     $0890,x
        cmp     #$4C
        beq     L85C7
        inx
        bpl     L85BD
L85C7:  lda     #$D5
        sta     $0891,x
        lda     #$DF
        sta     $0892,x
        rts

L85D2:  inc     $94
        dex
        bne     L85A3
        rts

L85D8:  jmp     LDF00

L85DB:  tya
        pha
        txa
        ldx     #$F4
L85E0:  lda     #$22
        sta     $AF
        .byte   $85
L85E5:  .byte   $AE
L85E6:  ldy     #$00
L85E8:  lda     ($AE),y
        cmp     L8617,y
        bne     L8605
        iny
        cpy     #$09
        bne     L85E8
        dec     $AF
        ldy     #$EB
L85F8:  lda     L8529,y
        sta     ($AE),y
        iny
        cpy     #$EE
        bne     L85F8
        jmp     L0C00

L8605:  inc     $AE
        bne     L85E6
        inc     $AF
        lda     $AF
        cmp     #$28
        bne     L85E6
        jmp     L0C00

        jmp     LDFCF

L8617:  ldy     #$00
        jsr     BASIN
        sta     ($6A),y
        iny
        .byte   $D0
L8620:  ldx     $6A
        stx     $FD
        ldx     $6B
        stx     $FE
        lda     $68
        and     #$0F
        tay
        lda     $68
        lsr     a
        lsr     a
        lsr     a
        lsr     a
        sta     $A3
        lda     $69
        asl     a
        asl     a
        asl     a
        asl     a
        ora     $A3
        clc
        adc     #$04
        cmp     #$12
        bne     L8646
        iny
        iny
L8646:  tax
        jsr     L9004
        jmp     setHIRAM

L864D:  ldx     #$DF
        lda     #$EE
        sta     $03DC
        stx     $03DD
        ldx     #$FE
        lda     #$BC
        stx     $0317
        sta     BRKVec
        jmp     L034B

L8664:  ldx     #$02
L8666:  lda     L8672,x
        sta     $737F,x
        dex
        .byte   $10
L866E:  .byte   $F7
        jmp     L7300

L8672:  jmp     LDFF4

L8675:  pha
        ldy     #$0D
L8678:  ldx     L86D7,y
        lda     $00,x
        pha
        lda     L86E4,y
        sta     $00,x
        dey
        bne     L8678
        lda     VIC_IMR
        pha
        lda     VIC_SPR_ENA
        pha
        lda     #$7F
        sta     CIA1_ICR
        ldx     #$FE
        lda     #$BC
        sty     VIC_IMR
        sty     VIC_SPR_ENA
        lda     $C2
        sta     $FD
        ldx     $C3
        stx     $FE
        lda     $BE
        jsr     L86EE
        tay
        lda     $BF
        jsr     L86EE
        tax
        jsr     L9004
        pla
        sta     VIC_SPR_ENA
        pla
        sta     VIC_IMR
        ldy     #$F3
L86BE:  ldx     L85E5,y
        pla
        sta     $00,x
        iny
        bne     L86BE
        pla
        tax
        lda     #$73
        pha
        inc     $C3
        lda     #$99
        pha
        lda     $A5
        stx     $A5
        .byte   $4C
        .byte   $36
L86D7:  .byte   $DF
        bcc     L866E
        sta     $A3,x
        lda     $A6
        .byte   $A7
        lda     $AEBA,y
        .byte   $AF
        .byte   $FD
L86E4:  inc     a:$00,x
        brk
        brk
        brk
        brk
        brk
        brk
        php
L86EE:  cmp     #$10
        bcc     L8702
        cmp     #$20
        bcc     L86FF
        cmp     #$30
        bcc     L86FC
        sbc     #$06
L86FC:  sec
        sbc     #$06
L86FF:  sec
        sbc     #$06
L8702:  rts

L8703:  jsr     L80C6
L8706:  .byte   $20
L8707:  ror     $DF,x
        stx     TXTPTR
        sty     $7B
        jsr     L0073
        tax
        beq     L8706
        php
        ldx     #$FF
        stx     $3A
        ldx     #$07
L871A:  cmp     L8738,x
        beq     L8726
        dex
        bpl     L871A
        plp
        jmp     LDF70

L8726:  plp
        lda     #$08
        cmp     DEVNUM
        bcc     L872F
        sta     DEVNUM
L872F:  lda     L8740,x
        pha
        lda     L8748,x
        pha
        rts

L8738:  and     $2F
        rol     $2440,x
        and     ($5C,x)
        .byte   $5F
L8740:  .byte   $87
        .byte   $87
        .byte   $87
        .byte   $87
        .byte   $87
        .byte   $93
        dey
        .byte   $87
L8748:  pla
        .byte   $4F
        bvs     L87BC
        ldx     $32
        txs
        eor     $20,x
        .byte   $44
        dey
        jmp     LE171

        jsr     L8844
        ldx     #$03
L875B:  lda     L8765,x
        pha
        dex
        bpl     L875B
        jmp     delayedcli

L8765:  cli
        sbc     (L0073,x)
        ldy     $20
        .byte   $44
        dey
        inc     SECADR
        jmp     LE171

        jsr     L8844
        ldy     #$00
        lda     (FNAM),y
        cmp     #$24
        beq     L87AE
        jsr     L8785
        jmp     L8703

L8782:  jsr     L8A92
L8785:  lda     #$0F
        jsr     L8878
        lda     DEVNUM
        jsr     LDF7D
        lda     #$FF
        jsr     LDF83
L8794:  jsr     LDF95
        cmp     #$0D
        beq     L87A1
        jsr     LDF89
        jmp     L8794

L87A1:  jsr     LDF89
        jmp     LDF8F

        jsr     L8844
        dec     FNAM
        inc     FNAM_LEN
L87AE:  jsr     L87CA
        jmp     L8703

L87B4:  lda     #$07
L87B6:  ldy     #$02
        ldx     #$00
        .byte   $20
        .byte   $BD
L87BC:  .byte   $FF
        jmp     L886C

L87C0:  lda     #$24
        sta     BASIC_BUF
        lda     #$01
        jsr     L87B6
L87CA:  lda     #$00
        jsr     L8878
        lda     DEVNUM
        jsr     LDF7D
        lda     SECADR
        jsr     LDF83
        lda     #$00
        sta     $90
        ldy     #$05
L87DF:  jsr     LDF95
        ldx     $90
        bne     L880D
        dey
        bne     L87DF
        tay
        jsr     LDF95
        pha
        tya
        tax
        pla
        jsr     LDFA5
        lda     #$20
L87F6:  jsr     LDF89
        jsr     LDF95
        ldx     $90
        bne     L880D
        cmp     #$00
        bne     L87F6
        lda     #$0D
        jsr     LDF89
        ldy     #$03
        bne     L87DF
L880D:  jsr     LDF8F
        lda     #$EF
        and     SECADR
        ora     #$E0
        tax
        jsr     L8273
        jmp     LDF5E

L881D:  sta     L02DF
        jsr     L8844
        ldy     #$00
        sty     FREKZP
L8827:  lda     (FNAM),y
        sta     $02E2,y
        cmp     #$2C
        bne     L8832
        dec     FREKZP
L8832:  iny
        cpy     FNAM_LEN
        bne     L8827
        lda     #$30
        sta     $02E0
        lda     #$3A
        sta     $02E1
        lda     FREKZP
        rts

L8844:  jsr     L0073
        bne     L8850
        ldx     TXTPTR
        ldy     $7B
        jmp     SETNAM

L8850:  cmp     #$22
        bne     L8857
        jsr     L0073
L8857:  ldx     TXTPTR
        ldy     #$00
L885B:  iny
        lda     (TXTPTR),y
        cmp     #$22
        beq     L8866
        cmp     #$00
        bne     L885B
L8866:  tya
        ldy     #$02
        jsr     SETNAM
L886C:  lda     #$01
        ldy     #$00
        ldx     DEVNUM
        jsr     SETLFS
        lda     #$00
        rts

L8878:  sta     SECADR
        jmp     LDF9B

L887D:  ldx     #$09
        stx     KEY_COUNT
L8881:  lda     L8891,x
        sta     KEY_BUF_m1,x
        dex
        bne     L8881
        lda     #$B8
        sta     $028F
        .byte   $4C
        .byte   $34
L8891:  .byte   $DF
        and     $30
        .byte   $3A
        rol     a
        ora     $5552
        .byte   $4E
        .byte   $0D
L889B:  lda     #$03
        sta     $02F8
        jsr     L968D
        jsr     L965A
        lda     #$A1
        ldx     #$8D
        jsr     L8D3E
        ldy     #$88
        ldx     #$B7
        jsr     L890B
        jmp     L889B

        eor     ($F0,x)
        dey
        eor     $02
        sty     $7543
        .byte   $89
        .byte   $44
        asl     $4299
        bit     $4699
        cmp     #$88
        brk
L88CA:  lda     #$EA
        ldx     #$8D
        jsr     L8D3E
        ldy     #$88
        ldx     #$DB
        jsr     L890B
        jmp     L88CA

        eor     ($F0,x)
        dey
        .byte   $42
        txs
        dey
        .byte   $43
        and     #$8B
        .byte   $44
        tay
        .byte   $8B
        eor     FNAM_LEN
        .byte   $8B
        lsr     $C9
        .byte   $8B
        .byte   $47
        cld
        .byte   $8B
        brk
        jsr     L87C0
L88F4:  lda     #$00
        sta     KEY_COUNT
        ldx     #$8F
        lda     #$0A
        jsr     L8D3E
L88FF:  jsr     L8936
        cmp     #$85
        beq     L896D
        cmp     #$20
        bne     L88FF
        rts

L890B:  sty     $AF
        stx     $AE
        lda     #$03
        sta     $02F8
L8914:  jsr     L8936
        tax
        cmp     #$85
        beq     L896D
        ldy     #$00
L891E:  txa
        cmp     ($AE),y
        bne     L892D
        iny
        iny
        lda     ($AE),y
        pha
        dey
        lda     ($AE),y
        pha
        rts

L892D:  iny
        iny
        iny
        lda     ($AE),y
        bne     L891E
        beq     L8914
L8936:  jsr     LDFDB
        cmp     #$00
        beq     L8936
        rts

L893E:  ldy     #$00
        sty     CURS_FLAG
        iny
        sty     CURS_BLINK
L8945:  jsr     LDFDB
        cmp     #$00
        beq     L8945
        ldy     #$01
        sty     CURS_BLINK
L8950:  jsr     LDF14
        ldy     CURS_STATE
        bne     L8950
        inc     CURS_FLAG
        rts

L895A:  jsr     L8D3E
        jsr     L9C22
        lda     BASIC_BUF
        cmp     #$20
        bne     L896C
        lda     $0201
        beq     L896D
L896C:  rts

L896D:  ldx     #$FD
        txs
        jmp     L889B

        pla
        pla
        rts

L8976:  ldx     #$8E
        lda     #$5A
        jsr     L8D3E
        ldy     #$89
        ldx     #$87
        jsr     L890B
        jmp     L8976

        eor     ($F0,x)
        dey
        .byte   $42
        .byte   $72
        .byte   $89
        .byte   $44
        ldx     $89
        eor     $29
        .byte   $8B
        lsr     $62
        .byte   $8B
        .byte   $43
        sta     $89,y
        ldx     #$8E
        lda     #$AB
        jsr     L8D3E
        jsr     L8AD3
        jmp     L89B4

        ldx     #$8E
        lda     #$88
        jsr     L8D3E
        jsr     L8A75
        jsr     L8ADE
L89B4:  ldx     #$01
        stx     $02CE
        stx     $45
        ldy     #$00
        sty     $46
        ldx     #$00
        ldy     #$0B
        stx     $61
        sty     $62
L89C7:  jsr     L965A
        jsr     L8A75
L89CD:  ldx     $45
        ldy     $46
        jsr     L8B03
        bcs     L89F2
        jsr     L8AF1
        lda     #$24
        sta     $A5
        lda     $45
        jsr     L9670
        lda     $46
        jsr     L9670
        ldx     #$00
L89E9:  lda     $0A00,x
        jsr     L9670
        inx
        bne     L89E9
L89F2:  ldx     $45
        lda     $46
        clc
        adc     #$0B
        cmp     L8AAF,x
        bcc     L8A01
        sbc     L8AAF,x
L8A01:  sta     $46
        bne     L8A0D
        inc     $45
        lda     #$FF
        cpx     #$23
        bcs     L8A16
L8A0D:  lda     $02B7
        cmp     #$FE
        bcc     L89CD
        lda     #$80
L8A16:  jsr     L9670
        jsr     L968D
        dec     $02CE
        bmi     L8A3C
        jsr     L8D3A
        jsr     L8936
        cmp     #$59
        bne     L8A3C
        ldx     #$06
L8A2D:  lda     L8A6E,x
        sta     BASIC_BUF,x
        dex
        bpl     L8A2D
        jsr     L87B4
        jsr     L8782
L8A3C:  jsr     L8A92
L8A3F:  jsr     L96B3
        tay
        bpl     L8A56
        cmp     #$80
        bne     L8A4C
        jmp     L89C7

L8A4C:  ldx     #$8F
        lda     #$9C
        jsr     L8D3E
        jmp     L88F4

L8A56:  sta     $45
        jsr     L96B3
        sta     $46
        ldx     #$00
L8A5F:  jsr     L96B3
        sta     $0A00,x
        inx
        bne     L8A5F
        jsr     L901F
        jmp     L8A3F

L8A6E:  lsr     $3A30
        .byte   $44
        bit     $3030
L8A75:  lda     $02F8
        cmp     #$01
        beq     L8A8B
        ldx     #$8F
        lda     #$5B
        jsr     L8D3E
        lda     #$01
        sta     $02F8
        jsr     L88F4
L8A8B:  ldx     #$8F
        lda     #$86
        jmp     L8D3E

L8A92:  lda     $02F8
        cmp     #$02
        beq     L8AA8
        ldx     #$8F
        lda     #$67
        jsr     L8D3E
L8AA0:  lda     #$02
        sta     $02F8
        jsr     L88F4
L8AA8:  ldx     #$8F
        lda     #$8D
        jmp     L8D3E

L8AAF:  brk
        ora     $15,x
        ora     $15,x
        ora     $15,x
        ora     $15,x
        ora     $15,x
        ora     $15,x
        ora     $15,x
        ora     $15,x
        ora     $13,x
        .byte   $13
        .byte   $13
        .byte   $13
        .byte   $13
        .byte   $13
        .byte   $13
        .byte   $12
        .byte   $12
        .byte   $12
        .byte   $12
        .byte   $12
        .byte   $12
        ora     ($11),y
        ora     ($11),y
        .byte   $11
L8AD3:  ldx     #$8F
        lda     #$00
L8AD7:  sta     $0900,x
        dex
        bne     L8AD7
        rts

L8ADE:  lda     #$00
        sta     $FD
        lda     #$09
        sta     $FE
        ldx     #$12
        ldy     #$00
        jmp     L9004

L8AED:  sty     $46
        stx     $45
L8AF1:  lda     #$0A
        sta     $FE
        lda     #$00
        sta     $FD
        sta     $0301
        ldy     $46
        ldx     $45
        jmp     L9004

L8B03:  lda     #$09
        sta     $AF
        txa
        asl     a
        asl     a
        sta     $AE
        tya
        sec
L8B0E:  inc     $AE
        sbc     #$08
        bcs     L8B0E
        adc     #$08
        tax
        ldy     #$00
        lda     L8B22,x
        and     ($AE),y
        cmp     L8B22,x
        rts

L8B22:  ora     ($02,x)
        .byte   $04
        php
        bpl     L8B48
        rti

        .byte   $80
        ldx     #$8E
        lda     #$B8
        jsr     L9085
        jsr     L9CE5
        lda     #$00
        sta     SECADR
        ldx     #$00
        ldy     #$0B
        jsr     LDFE8
        sty     $02D5
        stx     $02D4
        jsr     L8A92
L8B48:  lda     $61
        sta     $0AFE
        lda     $62
        sta     $0AFF
        lda     #$0A
        sta     $AD
        lda     #$FE
        sta     $AC
        ldy     $02D5
        ldx     $02D4
        jmp     L9C76

        ldx     #$8F
        lda     #$D7
        jsr     L895A
        lda     #$4E
        jsr     L881D
        bmi     L8B90
        iny
        iny
        iny
        sty     FNAM_LEN
        ldx     #$8F
        lda     #$EC
        jsr     L895A
        ldy     FNAM_LEN
        lda     BASIC_BUF
        sta     $02E0,y
        lda     $0201
        sta     $02E1,y
        lda     #$2C
        sta     L02DF,y
L8B90:  ldx     #$8F
        lda     #$78
        jsr     L8D3E
L8B97:  lda     FNAM_LEN
        clc
        adc     #$03
        ldy     #$02
        ldx     #$DF
        jsr     SETNAM
        jsr     L8AA0
        jmp     L8785

        ldx     #$8E
        lda     #$C4
        jsr     L895A
        lda     #$53
        jsr     L881D
        jmp     L8B97

        ldx     #$8E
        lda     #$CB
        jsr     L9085
        lda     #$40
        ora     $0A00,y
L8BC4:  sta     $0A00,y
        jmp     L901F

        ldx     #$8E
        lda     #$C9
        jsr     L9085
        lda     #$BF
        and     $0A00,y
        jmp     L8BC4

        ldx     #$8E
        lda     #$D0
        jsr     L9085
        ldx     #$8E
        lda     #$F5
        jsr     L895A
        jsr     L8844
        ldx     $02F9
        ldy     #$00
L8BEF:  lda     (FNAM),y
        cpy     FNAM_LEN
        bcc     L8BF7
        lda     #$A0
L8BF7:  sta     $0A03,x
        iny
        inx
        cpy     #$10
        bne     L8BEF
        jmp     L901F

        jsr     L8D90
        ldx     #$8F
        lda     #$B7
        jsr     L8D3E
        lda     #$12
        sta     $45
        lda     #$02
        sta     $46
        jsr     L8CD1
L8C18:  ldx     #$8F
        lda     #$B7
        jsr     L8D3E
        lda     $02F9
        and     #$07
        sta     FREKZP
        asl     a
        adc     FREKZP
        adc     #$06
        tay
        lda     $02F9
        lsr     a
        lsr     a
        lsr     a
        clc
        adc     #$04
        tax
        jsr     LDFE2
        jsr     L893E
        cmp     #$52
        bne     L8C46
        jsr     L8CD1
        jmp     L8C18

L8C46:  cmp     #$57
        bne     L8C61
        jsr     L8CFA
        ldx     #$0A
        stx     $62
        lda     #$00
        sta     $61
        sta     $02F9
        jsr     L901F
        jsr     L8CE2
        jmp     L8C18

L8C61:  cmp     #$85
        beq     L8C69
        cmp     #$51
        bne     L8C6A
L8C69:  rts

L8C6A:  cmp     #$1D
        bne     L8C89
L8C6E:  lda     #$01
L8C70:  clc
        adc     $02F9
        cmp     #$80
        and     #$7F
        sta     $02F9
        bcc     L8C18
        lda     $61
        eor     #$80
        sta     $61
        jsr     L8CE2
        jmp     L8C18

L8C89:  cmp     #$9D
        bne     L8C91
        lda     #$FF
        bne     L8C70
L8C91:  cmp     #$11
        bne     L8C99
        lda     #$08
        bne     L8C70
L8C99:  cmp     #$91
        bne     L8CA1
        lda     #$F8
        bne     L8C70
L8CA1:  jsr     L9A73
        bcs     L8CCE
        pha
        jsr     L9608
        pla
        asl     a
        asl     a
        asl     a
        asl     a
        sta     FREKZP
        jsr     L893E
        jsr     L9A73
        bcs     L8CCE
        and     #$0F
        ora     FREKZP
        pha
        lda     $61
        ora     $02F9
        tax
        pla
        sta     $0A00,x
        jsr     L8CE2
        jmp     L8C6E

L8CCE:  jmp     L8C18

L8CD1:  jsr     L8CFA
        ldy     #$0A
        ldx     #$00
        sty     $62
        stx     $61
        stx     $02F9
        jsr     L8AF1
L8CE2:  jsr     L968D
        ldx     #$0A
        lda     $61
        clc
        adc     #$7F
        jsr     L9704
        ldy     #$00
        ldx     #$03
        clc
        jsr     LDFE2
        jmp     L9970

L8CFA:  ldx     #$8F
        lda     #$48
        jsr     L8D3E
        lda     $45
        jsr     L8D26
        cmp     #$01
        bcc     L8CFA
        cmp     #$24
        bcs     L8CFA
        sta     $45
L8D10:  ldx     #$8F
        lda     #$3D
        jsr     L8D3E
        lda     $46
        jsr     L8D26
        ldy     $45
        cmp     L8AAF,y
        bcs     L8D10
        sta     $46
        rts

L8D26:  jsr     L95FF
        lda     #$9D
        jsr     LDF89
        jsr     LDF89
        jsr     L9C29
        jsr     L9B54
        lda     $61
        rts

L8D3A:  ldx     #$8E
        lda     #$9F
L8D3E:  sta     $AE
        stx     $AF
L8D42:  jsr     L8D95
        beq     L8D76
        bpl     L8D5D
        tax
        jsr     L8D95
        lda     $AE
        pha
        lda     $AF
        pha
        tya
        jsr     L8D3E
        pla
        tax
        pla
        jmp     L8D3E

L8D5D:  cmp     #$0C
        bne     L8D68
        lda     #$93
        jsr     LDF89
        lda     #$0D
L8D68:  cmp     #$01
        beq     L8D77
        cmp     #$02
        beq     L8D85
        jsr     LDF89
        jmp     L8D42

L8D76:  rts

L8D77:  jsr     L8D95
        tax
        jsr     L8D95
        clc
        jsr     LDFE2
        jmp     L8D42

L8D85:  jsr     L9480
        lda     CURS_X
        cmp     #$26
        bcc     L8D85
        bcs     L8D42
L8D90:  lda     #$93
        jmp     LDF89

L8D95:  ldy     #$00
        lda     ($AE),y
        inc     $AE
        bne     L8D9F
        inc     $AF
L8D9F:  tay
        rts

        .byte   $0C
        stx     L8E40
        and     ($8D),y
        .byte   $DF
        .byte   $42
        rol     L8E20
        .byte   $4F
        .byte   $42
        eor     ($53,x)
        eor     #$43
        sta     $43DF
        rol     L8F20
        .byte   $37
        sta     $44DF
        rol     $4420
        eor     #$53
        eor     ($42,x)
        jmp     L2045

        lsr     $41
        .byte   $53
        .byte   $54
        jmp     L414F

        .byte   $44
        sta     $45DF
        rol     L8F20
        .byte   $02
        sta     $46DF
        rol     L8E20
        jsr     L0D0D
        brk
        ora     $200D
        jsr     L2020
        jsr     L2020
        jsr     L0C00
        stx     L8E20
        .byte   "1"
        .byte   $8D,$DF
        .byte   "B. "
        .byte   $8E
        .byte   "O"
        .byte   $8E
        .byte   "@"
        .byte   $8D,$DF
        .byte   "C. "
        .byte   $8E,$D5,$8D,$DF
        .byte   "D. "
        .byte   $8E,$E5,$8E,$D7,$8D,$DF
        .byte   "E. "
        .byte   $8E
        .byte   "*F. UN"
        .byte   $8E
        .byte   "*G. "
        .byte   $8E,$ED,$8E,$D7,$0D,$0D,$00
L8E20:  .byte   $8E,$D9
        .byte   "UTILITY"
        .byte   $00,$8E,$DF,$8E,$D7,$8D,$DF,$00
        .byte   $8D,$DF
        .byte   "A. DIRECTORY"

        .byte   $00
L8E40:  .byte   "THE FIRST MENU"

        .byte   $00
        .byte   "RETURN TO "

        .byte   $00,$0C,$8F
        .byte   "7"
        .byte   $8E
        .byte   "1"
        .byte   $8D,$DF
        .byte   "B. "
        .byte   $8E
        .byte   "O"
        .byte   $8E
        .byte   "@"
        .byte   $8D,$DF
        .byte   "C. "
        .byte   $8E,$AC,$8D,$DF
        .byte   "D. "
        .byte   $8E,$89,$8D,$DF
        .byte   "E. "
        .byte   $8E,$D5,$8D,$DF
        .byte   "F. "
        .byte   $8E,$97,$8F,$CD,$0D,$00,$0C
        .byte   "BAM COPY"
        .byte   $00,$8E,$97,$8F,$CD,$00
        .byte   "FORMAT "
        .byte   $00,$8E,$97,$8F
        .byte   "i(Y/N)?"
        .byte   $0D,$00,$0C,$8F
        .byte   "7ENTIRE "
        .byte   $8F,$CD,$00,$8F
        .byte   "7WHICH "
        .byte   $8E,$D9,$0D,$00,$8E,$E5,$8E,$BA
        .byte   $00
        .byte   "UN"
        .byte   $8E,$DF,$8E,$BA,$00,$8E,$ED,$8E
        .byte   $BA,$00,$8F
        .byte   "7A FILE "
        .byte   $00
        .byte   "LOCK "
        .byte   $00
        .byte   "DELETE "
        .byte   $00
        .byte   "RENAME "
        .byte   $00,$8F,$FD,$8E,$FB,$0D,$00,$8E
        .byte   $D9
        .byte   "NAME"
        .byte   $00
        .byte   "EDIT "
        .byte   $8F,$CD,$00,$0D
        .byte   "PRESS SPACE TO CONTIN"


L8F20:  .byte   "UE, F1 TO ABORT"

        .byte   $0D,$00
        .byte   "TRACK"
        .byte   $00
        .byte   "COPY "
        .byte   $00,$01,$01,$1B
        .byte   "SECTOR "
        .byte   $00,$01,$01,$11,$8F
        .byte   "1"
        .byte   $02,$01,$01,$17,$00,$0D
        .byte   "INSERT "
        .byte   $00,$8F
        .byte   "RSOURCE "
        .byte   $8F,$CD,$00,$8F
        .byte   "RDESTINATION "

        .byte   $8F,$CD,$00,$8F
        .byte   "R"
        .byte   $8F,$CD
        .byte   "TO ERASE"
        .byte   $0D,$00
        .byte   "READ"
        .byte   $8F,$91,$00
        .byte   "WRITING . . ."

        .byte   $0D,$00,$0D,$8F
        .byte   "7COMPLETED"

        .byte   $0D,$00,$8E,$D9
        .byte   "NOT FOUND"

        .byte   $0D,$00,$13,$0D,$8F,$02,$0D
        .byte   "READ WRITE QUIT"

        .byte   $0D,$00
        .byte   "DISKETTE "

        .byte   $00,$8F,$FD
        .byte   "TITLE FOR THIS "

        .byte   $8F,$CD,$0D,$00,$8F,$FD
        .byte   "I.D. "
        .byte   $8F,$DF,$00
        .byte   "ENTER "
        .byte   $00,$8F,$F6
        .byte   "NEW "
        .byte   $00
L9004:  .byte   " "
        and     $A990,y
        .byte   $02
        jsr     L8290
        jsr     L8558
        ldy     #$00
L9011:  sty     $90
        jsr     L8425
        ldy     $90
        sta     ($FD),y
        iny
        bne     L9011
        clc
        rts

L901F:  ldx     $45
        ldy     $46
        jsr     L9039
        lda     #$01
        jsr     L8290
        ldx     #$00
L902D:  lda     $0A00,x
        jsr     L8290
        inx
        bne     L902D
        jmp     L8558

L9039:  tya
        pha
        txa
        pha
        lda     CIA2_PRA
        and     #$30
        cmp     #$20
        bne     L9074
L9046:  lda     CIA2_PRA
        cmp     CIA2_PRA
        bne     L9046
        cmp     CIA2_PRA
        bne     L905D
        cmp     CIA2_PRA
        bne     L905D
        cmp     CIA2_PRA
        beq     L9074
L905D:  and     #$0F
        sta     CIA2_PRA
        ldx     #$07
L9064:  dex
        bne     L9064
        ora     #$20
        sta     CIA2_PRA
        ldx     #$1E
L906E:  dex
        bne     L906E
        jmp     L9077

L9074:  jsr     L90F7
L9077:  jsr     L9106
        jsr     L8558
        pla
        jsr     L8290
        pla
        jmp     L8290

L9085:  jsr     L895A
        jsr     L8844
        ldx     #$12
        ldy     #$01
L908F:  jsr     L8AED
        lda     #$0A
        sta     $AF
        lda     #$05
L9098:  sta     $AE
        tax
        lda     $09FD,x
        and     #$07
        beq     L90C7
        ldy     #$00
L90A4:  lda     (FNAM),y
        cmp     #$2A
        beq     L90E3
        cmp     ($AE),y
        beq     L90B8
        cmp     #$3F
        bne     L90C7
        lda     ($AE),y
        cmp     #$A0
        beq     L90C7
L90B8:  iny
        cpy     #$10
        beq     L90E3
        cpy     FNAM_LEN
        bcc     L90A4
        lda     ($AE),y
        cmp     #$A0
        beq     L90E3
L90C7:  lda     $AE
        clc
        adc     #$20
        bcc     L9098
        ldy     $0A01
        ldx     $0A00
        bne     L908F
        ldx     #$8F
        lda     #$AA
        jsr     L8D3E
        jsr     L88F4
        pla
        pla
        rts

L90E3:  ldx     $AE
        ldy     $AF
        lda     #$10
        jsr     SETNAM
        lda     $AE
        sec
        sbc     #$03
        sta     $02F9
        tay
        clc
        rts

L90F7:  jsr     L821C
        ldy     #$00
L90FC:  lda     L9212,y
        jsr     L8290
        iny
        bne     L90FC
        rts

L9106:  lda     VIC_CTRL1
        and     #$07
        eor     #$07
        sta     $94
        lda     #$83
        sta     $95
        lda     CIA2_PRA
        lsr     a
        lsr     a
        and     #$03
        eor     CIA2_PRA
        and     #$0F
        eor     #$FF
        sta     $A3
        rts

L9124:  jsr     L0180
        tax
        stx     $A3
        sec
        bcs     L9133
L912D:  jsr     L0180
        sta     BASIC_BUF,x
L9133:  dex
        bpl     L912D
        lda     #$08
        sta     $1800
        lda     $1C00
        ora     #$08
        sta     $1C00
        lda     #$00
        sta     $83
        lda     #$F0
        sta     $84
        cli
        .byte   $AD
L914D:  .byte   $4F
        .byte   $02
        ora     #$04
        and     #$FD
        sta     $024F
        jsr     LC146
        ldy     #$00
L915B:  lda     ($30),y
        sta     $0400,y
        iny
        bne     L915B
        sty     $83
        jsr     LDAC0
        lda     #$01
        sta     $1C
        sei
        lda     #$00
        sta     $7F
        sta     $028E
        sta     $FF
        beq     L91B6
        lda     #$08
        sta     $1800
        lda     $1C07
        ldy     #$01
        sty     $1C05
        sta     $1C07
        iny
        sty     $8B
L918B:  cli
        lda     #$80
        sta     PIO
L9190:  lda     PIO
        bmi     L9190
        cmp     #$01
        beq     L91B5
        dec     $8B
        bmi     L91B0
        bne     L91A2
        lda     #$C0
        sta     PIO
L91A2:  lda     $16
        sta     $12
        lda     $17
        sta     $13
L91AA:  lda     PIO
        bmi     L91AA
        bpl     L918B
L91B0:  ldx     #$02
        jmp     LE60A

L91B5:  sei
L91B6:  ldx     $0401
        stx     $09
        lda     $0400
        sta     $08
        beq     L91C4
        ldx     #$FF
L91C4:  stx     $15
        dex
        stx     $0401
        ldy     #$00
        lda     #$00
        sta     $1800
L91D1:  iny
        lda     $0400,y
        lsr     a
        lsr     a
        lsr     a
        lsr     a
        tax
        lda     #$01
        sta     $1800
L91DF:  bit     $1800
        bne     L91DF
        stx     $1800
        txa
        asl     a
        and     #$0F
        sta     $1800
        lda     $0400,y
        and     #$0F
        sta     $1800
        asl     a
        and     #$0F
        nop
        sta     $1800
        cpy     $15
        bne     L91D1
        lda     $0400
        beq     L9209
        jmp     L0554

L9209:  sta     $1800
        sta     $026C
        jmp     LC194

L9212:  jsr     L05D7
        sta     $08
        jsr     L05D7
        sta     $09
        jsr     L05D7
        sta     $8C
        cmp     #$01
        bne     L9230
        ldx     #$00
L9227:  jsr     L05D7
        sta     $0400,x
        inx
        bne     L9227
L9230:  lda     #$08
        sta     $1800
        lda     $1C00
        ora     #$08
        sta     $1C00
        lda     $1C07
        ldy     #$01
        sty     $1C05
        sta     $1C07
        iny
        sty     $8B
L924B:  cli
        lda     #$80
        ldy     $8C
        cpy     #$01
        bne     L9256
        lda     #$90
L9256:  sta     PIO
L9258:  lda     PIO
        bmi     L9258
        cmp     #$01
        beq     L927D
        dec     $8B
        bmi     L9278
        bne     L926A
        lda     #$C0
        sta     PIO
L926A:  lda     $16
        sta     $12
        lda     $17
        sta     $13
L9272:  lda     PIO
        bmi     L9272
        bpl     L924B
L9278:  ldx     #$02
        jmp     LE60A

L927D:  sei
        ldy     $8C
        cpy     #$01
        beq     L92B6
        ldy     #$00
        sty     $1800
L9289:  lda     $0400,y
        lsr     a
        lsr     a
        lsr     a
        lsr     a
        tax
        lda     #$01
        sta     $1800
L9296:  bit     $1800
        bne     L9296
        stx     $1800
        txa
        asl     a
        and     #$0F
        sta     $1800
        lda     $0400,y
        and     #$0F
        sta     $1800
        asl     a
        and     #$0F
        iny
        sta     $1800
        bne     L9289
L92B6:  lda     $1C00
        and     #$F7
        sta     $1C00
        lda     #$01
        ldx     #$08
        ldy     #$00
L92C4:  stx     $1800
        bit     $1800
        bmi     L92E8
        cli
        sei
        sty     $1800
        bit     $1800
        bmi     L92E8
        nop
        bne     L92C4
        lda     #$01
        ldx     #$0E
L92DD:  dex
        bne     L92DD
        bit     $1800
        beq     L92E8
        jmp     L0500

L92E8:  rts

        ldy     #$04
L92EB:  lda     #$04
L92ED:  bit     $1800
        bmi     L930F
        beq     L92ED
        lda     $1800
        lsr     a
        ror     $14
        lda     #$04
L92FC:  bit     $1800
        bmi     L930F
        bne     L92FC
        lda     $1800
        lsr     a
        ror     $14
        dey
        bne     L92EB
        lda     $14
        rts

L930F:  pla
        pla
        rts

        sei
        lda     #$08
        sta     $1800
        lda     #$01
L931A:  bit     $1800
        beq     L931A
        sta     $1800
        ldx     #$00
L9324:  jsr     L0180
        sta     L0500,x
        inx
        bne     L9324
        inx
        stx     $1C
        jmp     L0500

        sei
        cld
        lda     #$27
        sta     $A5
        lda     #$00
        sta     $02CC
        ldx     #$0D
L9340:  lda     L9372,x
        sta     $02BB,x
        dex
        bpl     L9340
        jsr     L968D
        jsr     L965A
        tsx
        stx     $02CD
        jmp     L9A27

L9356:  ldx     #$01
L9358:  lda     L936E,x
        sta     NMIVec,x
        sta     $FFFA,x
        sta     $FFFE,x
        lda     L9370,x
        sta     BRKVec,x
        dex
        bpl     L9358
        rts

L936E:  .byte   $BB
        .byte   $02
L9370:  .byte   $9E
        .byte   $94
L9372:  sei
        rti

        pha
        lda     #$40
        sta     $02BC
        jsr     sub_DF06
        jmp     L94A2

L9380:  jsr     L9B2D
        jsr     L96A8
        sta     $63
        tay
        lsr     a
        bcc     L9395
        ror     a
        bcs     L93A4
        cmp     #$A2
        beq     L93A4
        and     #$87
L9395:  lsr     a
        tax
        lda     L9D63,x
        bcc     L93A0
        lsr     a
        lsr     a
        lsr     a
        lsr     a
L93A0:  and     #$0F
        bne     L93A8
L93A4:  ldy     #$80
        lda     #$00
L93A8:  tax
        lda     L9DA7,x
        sta     $02CE
        and     #$03
        sta     $02CF
        tya
        and     #$8F
        tax
        tya
        ldy     #$03
        cpx     #$8A
        beq     L93CA
L93BF:  lsr     a
        bcc     L93CA
        lsr     a
L93C3:  lsr     a
        ora     #$20
        dey
        bne     L93C3
        iny
L93CA:  dey
        bne     L93BF
        pha
        lda     #$00
        sta     $65
        ldx     $02CF
        beq     L93E4
        jsr     L96A8
        sta     $64
        dex
        beq     L93E4
        jsr     L96A8
        sta     $65
L93E4:  pla
        rts

L93E6:  jsr     L9380
        tay
        lda     L9DC1,y
        sta     $02D0
        lda     L9E01,y
        sta     $02D1
        rts

L93F7:  jsr     L93E6
L93FA:  jsr     L95EC
        ldy     #$FF
L93FF:  iny
        lda     $63,y
        jsr     L95FF
        jsr     L9480
        cpy     $02CF
        bne     L93FF
        ldx     L95C2,y
        jsr     L9487
        lda     #$2F
        jsr     L9610
        jsr     L9480
        ldx     #$03
L941E:  lda     #$00
        ldy     #$05
L9422:  asl     $02D1
        rol     $02D0
        rol     a
        dey
        bne     L9422
        adc     #$3F
        jsr     L9610
        dex
        bne     L941E
        jsr     L9485
        ldy     $02CF
        ldx     #$06
L943C:  cpx     #$03
        beq     L945D
L9440:  asl     $02CE
        bcc     L9453
        lda     L9DB4,x
        jsr     L9610
        lda     L9DBA,x
        beq     L9453
        jsr     L9610
L9453:  dex
        bne     L943C
        rts

L9457:  dey
        bmi     L9440
        jsr     L95FF
L945D:  lda     $02CE
        cmp     #$E8
        lda     $63,y
        bcc     L9457
        jsr     L948E
        tax
L946B:  tya
L946C:  jsr     L95FF
        txa
        jmp     L95FF

L9473:  txa
        bne     L9477
        dey
L9477:  dex
        jsr     L946B
        lda     #$3A
        jmp     L9610

L9480:  lda     #$20
        jmp     L9610

L9485:  ldx     #$03
L9487:  jsr     L9480
        dex
        bne     L9487
        rts

L948E:  clc
        ldy     $02AD
        lda     $64
        bpl     L9497
        dey
L9497:  adc     $02AC
        bcc     L949D
        iny
L949D:  rts

        pla
        tay
        pla
        tax
L94A2:  pla
        sta     $02C9
        stx     $02CA
        sty     $02CB
        pla
        sta     $02CC
        pla
        sta     $61
        pla
        sta     $62
        jsr     L968D
        tsx
        stx     $02CD
        jsr     L93F7
        jsr     L9B47
L94C3:  jsr     L956C
        jmp     L9A2D

L94C9:  jsr     L93F7
        pla
        sta     $02D0
        pla
        sta     $02D1
        ldx     #$13
L94D6:  lda     L95AE,x
        sta     L02DF,x
        dex
        bpl     L94D6
        lda     $63
        beq     L94C3
        ldy     $02CF
        cmp     #$20
        beq     L9548
        cmp     #$60
        beq     L9520
        cmp     #$4C
        beq     L9556
        cmp     #$6C
        beq     L952E
        cmp     #$40
        beq     L951C
        and     #$1F
        eor     #$17
        cmp     #$07
        beq     L9505
L9502:  lda     $63,y
L9505:  sta     $02E4,y
        dey
        bpl     L9502
        lda     $02CC
        pha
        lda     $02C9
        pha
        ldx     $02CA
        ldy     $02CB
        jmp     L02DF

L951C:  pla
        sta     $02CC
L9520:  pla
        sta     $02AC
        pla
        sta     $02AD
        jsr     L96A8
        jmp     L9560

L952E:  lda     $64
        ldy     $65
        sta     $02AC
        sty     $02AD
        jsr     L96A8
        tax
        jsr     L96A8
        sta     $02AD
        stx     $02AC
        jmp     L9560

L9548:  ldy     $02AD
        ldx     $02AC
        bne     L9551
        dey
L9551:  dex
        tya
        pha
        txa
        pha
L9556:  ldy     $65
        lda     $64
L955A:  sty     $02AD
        sta     $02AC
L9560:  tsx
        stx     $02CD
        lda     $02D1
        pha
        lda     $02D0
        pha
L956C:  jsr     L95DB
        ldx     #$00
L9571:  lda     #$20
        jsr     L9610
        lda     L95D6,x
        jsr     L9610
        lda     #$3D
        jsr     L9610
        lda     $02C9,x
        jsr     L95FF
        inx
        cpx     #$05
        bne     L9571
        ldx     #$00
        rts

L958F:  clc
        lda     $64
        jsr     L948E
        jmp     L955A

L9598:  sta     $02C9
        php
        pla
        sta     $02CC
        stx     $02CA
        sty     $02CB
        tsx
        stx     $02CD
        cld
        jmp     L9560

L95AE:  jsr     setHIRAM
        pla
        plp
        nop
        nop
        nop
        jsr     sub_DF06
        jmp     L9598

        jsr     sub_DF06
        jmp     L958F

L95C2:  ora     #$06
        .byte   $03
        lda     #$02
        sta     $62
        lda     #$C9
        sta     $61
        jsr     L965A
        jsr     L9628
        jmp     L956C

L95D6:  eor     ($58,x)
        eor     $5350,y
L95DB:  lda     #$0D
L95DD:  jmp     L9610

L95E0:  jsr     L95DB
        ldy     $02AD
        ldx     $02AC
        jmp     L95F5

L95EC:  jsr     L95DB
        ldy     $02DD
        ldx     $02DC
L95F5:  jsr     L9480
        jsr     L946B
        lda     #$3A
        bne     L95DD
L95FF:  pha
        lsr     a
        lsr     a
        lsr     a
        lsr     a
        jsr     L960A
        pla
L9608:  and     #$0F
L960A:  cmp     #$0A
        sed
        adc     #$30
        cld
L9610:  jmp     LDF89

L9613:  ldx     #$08
L9615:  asl     a
        pha
        lda     #$30
        bcc     L961D
        lda     #$31
L961D:  jsr     L9610
        pla
        dex
        bne     L9615
        rts

        jsr     L9657
L9628:  jsr     L9B54
        bcs     L9644
        ldy     $02B6
        dey
        tya
        ora     $02B7
        bne     L963C
        lda     $61
        jsr     L9645
L963C:  lda     $61
        jsr     L9670
        jmp     L9628

L9644:  rts

L9645:  sta     $A5
        rts

        jsr     L96E2
L964B:  jsr     L96A8
        jsr     L9670
        jsr     L96D0
        bcs     L964B
        rts

L9657:  jsr     L9B54
L965A:  ldx     #$09
L965C:  lda     L967E,x
        sta     L02B1,x
        dex
        bpl     L965C
        lda     $61
        sta     $02B6
        lda     $62
        sta     $02B7
        rts

L9670:  ldy     $A5
        jsr     L02B1
        inc     $02B6
        bne     L967D
        inc     $02B7
L967D:  rts

L967E:  sty     PIO
        ldy     #$27
        sta     $FFFF
        sty     PIO
L9687:  rts

L9688:  jsr     L9B54
        bcs     L9687
L968D:  ldx     #$09
L968F:  lda     L96C6,x
        sta     L02A7,x
        dex
        bpl     L968F
        lda     $61
        sta     $02AC
        lda     $62
        sta     $02AD
        lda     #$00
        sta     $02D8
        rts

L96A8:  ldy     $A5
        lda     $02AD
        bpl     L96B5
        cmp     #$A0
        bcs     L96B5
L96B3:  ldy     #$24
L96B5:  jsr     L02A7
        inc     $02AC
        bne     L96C5
        inc     $02AD
        bne     L96C5
        inc     $02D8
L96C5:  rts

L96C6:  sty     PIO
        ldy     #$27
        lda     $FFFF
        sty     PIO
        rts

L96D0:  lda     $02D4
        cmp     $02AC
        lda     $02D5
        sbc     $02AD
        lda     #$00
        sbc     $02D8
        rts

L96E2:  jsr     L9657
L96E5:  jsr     L9688
        jsr     L9B54
        bcc     L9700
        lda     $02AC
        clc
        adc     #$1F
        ldx     $02AD
        bcc     L9704
        inx
        bne     L9704
        lda     #$FF
        tax
        bne     L9704
L9700:  ldx     $62
        lda     $61
L9704:  stx     $02D5
        sta     $02D4
        rts

        jsr     L96E2
        lda     #$AD
        sta     $02B5
L9713:  jsr     L9670
        sta     $61
        jsr     L96A8
        cmp     $61
        beq     L9740
        pha
        jsr     L95E0
        pla
        jsr     L95FF
        jsr     L9480
        lda     #$2F
        jsr     L9610
        jsr     L9480
        ldx     $02B6
        ldy     $02B7
        jsr     L9473
        lda     $61
        jsr     L95FF
L9740:  jsr     L96D0
        bcs     L9713
        rts

        jsr     L96E5
L9749:  jsr     L93F7
        jsr     L96D0
        bcs     L9749
        rts

        lda     #$21
        bit     $59A9
        bit     $4DA9
        sta     $02DE
        jsr     L982D
L9760:  jsr     L9813
        bcc     L9770
        lda     $02CE
        cmp     $02DE
        bne     L9770
        jsr     L93FA
L9770:  jsr     L96D0
        bcs     L9760
        rts

        jsr     L982D
L9779:  jsr     L9813
        bcc     L9790
        lda     $02CE
        cmp     #$4D
        beq     L978D
        cmp     #$59
        beq     L978D
        cmp     #$4A
        bne     L9790
L978D:  jsr     L93FA
L9790:  jsr     L96D0
        bcs     L9779
        rts

        jsr     L982D
L9799:  jsr     L93E6
        lda     $02CE
        cmp     #$21
        beq     L97B3
        cmp     #$9D
        beq     L97B9
        and     #$03
        beq     L97B3
        jsr     L9816
        bcc     L97B3
L97B0:  jsr     L93FA
L97B3:  jsr     L96D0
        bcs     L9799
        rts

L97B9:  jsr     L948E
        tax
        tya
        cpx     $02F6
        sbc     $02F7
        bcc     L97B3
        tya
        cpx     $02F4
        sbc     $02F5
        bcc     L97B0
        bne     L97B3
        cpx     $02F4
        beq     L97B0
        .byte   $D0
L97D7:  .byte   $DB
        jsr     L9B54
        lda     $61
        sta     $02D6
        lda     $62
        sta     $02D7
        jsr     L982D
L97E8:  jsr     L9813
        bcc     L980D
        lda     $02CF
        cmp     #$02
        bne     L980D
        jsr     L9B3A
        lda     $63
        jsr     L9670
        lda     $64
        sec
        sbc     $02D6
        jsr     L9670
        lda     $65
        sbc     $02D7
        jsr     L9670
L980D:  jsr     L96D0
        bcs     L97E8
        rts

L9813:  jsr     L93E6
L9816:  lda     $64
        cmp     $02F6
        lda     $65
        sbc     $02F7
        bcc     L982C
        lda     $02F4
        cmp     $64
        lda     $02F5
        sbc     $65
L982C:  rts

L982D:  jsr     L9B54
        lda     $61
        sta     $02F6
        lda     $62
        sta     $02F7
        jsr     L9B54
        lda     $61
        sta     $02F4
        lda     $62
        sta     $02F5
        jmp     L96E5

        jsr     L96E5
        ldx     #$FF
L984F:  inx
        stx     $02F3
        jsr     L9B54
        lda     $61
        ldx     $02F3
        sta     L02DF,x
        jsr     L9B54
        lda     $61
        ldx     $02F3
        sta     $02E9,x
        bcc     L984F
L986B:  jsr     L9B2D
        ldx     #$00
L9870:  jsr     L96A8
        cmp     L02DF,x
        bcc     L989C
        cmp     $02E9,x
        beq     L987F
        bcs     L989C
L987F:  inx
        cpx     $02F3
        bne     L9870
        jsr     L9B47
        jsr     L95E0
        ldx     $02F3
L988E:  jsr     L96A8
        jsr     L95FF
        lda     #$20
        jsr     L9610
        dex
        bne     L988E
L989C:  jsr     L9B47
        jsr     L96A8
        jsr     L96D0
        bcs     L986B
        rts

        pla
        pla
        jsr     L9688
L98AD:  ldx     #$04
L98AF:  lda     L9902,x
        sta     L02A7,x
        dex
        bpl     L98AF
        ldx     #$07
L98BA:  lda     L9907,x
        sta     $02AE,x
        dex
        bpl     L98BA
        ldx     $02CA
        ldy     $02CB
        lda     $02CC
        pha
        lda     $02C9
        pha
        lda     $A5
        jmp     L02A7

L98D6:  pla
        sta     $02C9
        pla
        sta     $02CC
        stx     $02CA
        sty     $02CB
        rts

L98E5:  stx     $02CA
        sty     $02CB
        sta     $02C9
        lda     #$26
L98F0:  jsr     L9645
        lda     #$30
        sta     $02CC
        jmp     L98AD

        jsr     L9688
        lda     #$27
        bne     L98F0
L9902:  sta     PIO
        pla
        plp
        .byte   $20
L9907:  php
        pha
        jsr     sub_DF06
        jmp     L98D6

        lda     #$94
        sta     $0300
        lda     #$E3
        sta     $0301
        lda     #$EB
        sta     $0290
        lda     #$48
        sta     $028F
        lda     #$A5
        sta     $0330
        lda     #$F4
        sta     $0331
        jsr     LDF5E
        lda     #$47
        sta     NMIVec
        lda     #$FE
        sta     $0319
        ldx     #$FB
        txs
        ldy     $0300
        lda     $0301
        bne     L9948
        jmp     coldstart

L9948:  sty     $02AC
        sta     $02AD
        lda     #$34
        sta     $02CC
        lda     #$27
        jsr     L9645
        stx     $02CA
        jmp     L98AD

        jsr     L9688
L9961:  jsr     L94C9
        jmp     L9961

        jsr     L9688
        jmp     L94C9

        jsr     L96E5
L9970:  jsr     L95E0
        jsr     L9B2D
L9976:  jsr     L96A8
        tay
        lda     $02AC
        eor     #$02
        ora     $02AD
        bne     L9986
        ldy     $A5
L9986:  tya
        jsr     L95FF
        jsr     L9480
        lda     $02AC
        and     #$07
        bne     L9976
        jsr     L9B47
        lda     #$2F
        jsr     L9610
L999C:  jsr     L96A8
        jsr     L9A0E
        lda     $02AC
        and     #$07
        bne     L999C
        jsr     L96D0
        bcs     L9970
L99AE:  rts

        jsr     L9B54
        jsr     L95DB
        lda     #$24
        jsr     L9610
        lda     $62
        ldx     $61
        jsr     L946C
        jsr     L9480
        lda     #$23
        jsr     L9610
        lda     $62
        pha
        ldx     $61
        txa
        pha
        lda     $62
        jsr     LDFA5
        jsr     L9480
        lda     #$25
        jsr     L9610
        pla
        sta     $61
        pla
        sta     $62
        jsr     L9613
        lda     $61
        jsr     L9613
        jsr     L9480
        lda     $61
        jmp     L9A0E

        jsr     L96E5
L99F6:  jsr     L95E0
L99F9:  jsr     L96A8
        jsr     L9A0E
        jsr     L96D0
        bcc     L9A0D
        lda     $02AC
        and     #$1F
        beq     L99F6
        bne     L99F9
L9A0D:  rts

L9A0E:  cmp     #$0D
        beq     L9A16
        cmp     #$8D
        bne     L9A18
L9A16:  lda     #$5F
L9A18:  cmp     #$94
        bne     L9A1E
        lda     #$20
L9A1E:  pha
        lda     #$01
        sta     $D8
        pla
        jmp     L9610

L9A27:  ldx     #$FF
        stx     $02CD
        txs
L9A2D:  jsr     L9356
        cld
        jsr     L95DB
        lda     #$2E
        jsr     L9610
        lda     #$00
        sta     $02DB
        jsr     L9C22
L9A41:  ldx     $02CD
        txs
        jsr     L9BE6
        ldy     #$1C
L9A4A:  dey
        bmi     L9A41
        cmp     L9E41,y
        bne     L9A4A
        jsr     L9BCE
        jmp     L9A41

L9A58:  rol     a
        rol     a
        rol     a
        rol     a
        ldx     #$04
L9A5E:  rol     a
        rol     $61
        rol     $62
        dex
        bne     L9A5E
        jsr     L9BDC
        jsr     L9A73
        bcc     L9A58
        clc
L9A6F:  dec     $02D2
        rts

L9A73:  jsr     L9A84
        bcc     L9A82
        sbc     #$47
        cmp     #$FA
        bcs     L9A82
        adc     #$47
L9A80:  sec
        rts

L9A82:  clc
        rts

L9A84:  cmp     #$30
        bcc     L9A80
        cmp     #$3A
        rts

L9A8B:  ldx     #$00
        stx     $61
        stx     $62
        bit     $02DB
        bpl     L9A99
        jmp     L9ACE

L9A99:  jsr     L9C14
        cmp     #$28
        beq     L9AB9
        cmp     #$27
        beq     L9AEE
        cmp     #$23
        beq     L9AF5
        cmp     #$24
        beq     L9A99
        cmp     #$22
        beq     L9AC9
        jsr     L9A73
        bcc     L9A58
        dec     $02D2
        rts

L9AB9:  jsr     L9B54
        jsr     L9C14
        cmp     #$29
        bne     L9AC5
        clc
        rts

L9AC5:  sec
        jmp     L9A6F

L9AC9:  lda     #$C0
        sta     $02DB
L9ACE:  jsr     L9BDC
        sta     $61
        cmp     #$22
        beq     L9ADD
        cmp     #$00
        beq     L9AE5
        clc
        rts

L9ADD:  lda     #$00
        sta     $02DB
        jmp     L9A8B

L9AE5:  sec
        lda     #$00
        sta     $02DB
        jmp     L9A6F

L9AEE:  jsr     L9BDC
        sta     $61
        clc
        rts

L9AF5:  jsr     L9BDC
        cmp     #$24
        beq     L9A99
        jsr     L9A84
        bcs     L9B29
        and     #$0F
        ldx     $61
        stx     $02D9
        ldx     $62
        stx     $02DA
        sta     $61
        lda     #$00
        sta     $62
        ldx     #$0A
L9B15:  clc
        lda     $02D9
        adc     $61
        sta     $61
        lda     $02DA
        adc     $62
        sta     $62
        dex
        bne     L9B15
        beq     L9AF5
L9B29:  clc
        jmp     L9A6F

L9B2D:  lda     $02AC
        sta     $02DC
        lda     $02AD
        sta     $02DD
        rts

L9B3A:  lda     $02DC
        sta     $02B6
        lda     $02DD
        sta     $02B7
        rts

L9B47:  lda     $02DC
        sta     $02AC
        lda     $02DD
        sta     $02AD
        rts

L9B54:  jsr     L9A8B
        bcs     L9B86
        bit     $02DB
        bmi     L9B86
L9B5E:  jsr     L9C14
        ldx     #$04
L9B63:  cmp     L9B87,x
        beq     L9B70
        dex
        bpl     L9B63
        jsr     L9A6F
        clc
        rts

L9B70:  lda     $62
        pha
        lda     $61
        pha
        txa
        pha
        jsr     L9A8B
        pla
        tax
        lda     L9B8C,x
        pha
        lda     L9B91,x
        pha
        rts

L9B86:  rts

L9B87:  .byte   $2B
        and     $3F26
        .byte   $21
L9B8C:  .byte   $9B
        .byte   $9B
        .byte   $9B
        .byte   $9B
        .byte   $9B
L9B91:  sta     TIME,x
        .byte   $AF
        lda     $68C3,y
        clc
        adc     $61
        tax
        pla
        adc     $62
        jmp     L9BA9

        pla
        sec
        sbc     $61
        tax
        pla
        sbc     $62
L9BA9:  stx     $61
        sta     $62
        jmp     L9B5E

        pla
        and     $61
        tax
        pla
        and     $62
        jmp     L9BA9

        pla
        eor     $61
        tax
        pla
        eor     $62
        jmp     L9BA9

        pla
        ora     $61
        tax
        pla
        ora     $62
        jmp     L9BA9

L9BCE:  tya
        asl     a
        tay
        lda     L9E63,y
        pha
        lda     L9E62,y
        pha
        ldy     #$00
        rts

L9BDC:  ldy     $02D2
        inc     $02D2
        lda     BASIC_BUF,y
        rts

L9BE6:  jsr     L9C03
        cmp     #$27
        bne     L9BF3
        jsr     L9C03
        jmp     L9BE6

L9BF3:  cmp     #$22
        bne     L9C02
L9BF7:  jsr     L9C03
        cmp     #$22
        beq     L9BE6
        cmp     #$00
        bne     L9BF7
L9C02:  rts

L9C03:  ldy     $02D3
        inc     $02D3
        lda     BASIC_BUF,y
        rts

        ldy     $02D3
        sty     $02D2
        rts

L9C14:  jsr     L9BDC
        ldy     #$1A
L9C19:  cmp     L9E47,y
L9C1C:  beq     L9C14
        dey
        bpl     L9C19
        rts

L9C22:  ldy     #$40
        sty     $02BC
        ldy     #$00
L9C29:  sty     KEY_COUNT
        jsr     LDF76
        stx     TXTPTR
        sty     $7B
        ldy     #$EA
        sty     $02BC
        ldy     #$00
        sty     $02D2
        sty     $02D3
        rts

        jsr     L9D2C
        jsr     L96E5
        jsr     L9C93
        lda     #$02
        ldy     #$01
        jsr     SETLFS
        jsr     L9D34
        lda     $02AC
        sta     $FD
        lda     $02AD
        sta     $FE
        lda     #$D8
        ldx     #$FF
        sta     $02AC
        stx     $02AD
        ldy     $02D5
        ldx     $02D4
        inx
        bne     L9C71
        iny
L9C71:  lda     #$FD
        jmp     L98E5

L9C76:  stx     $AE
        sty     $AF
        lda     #$61
        jsr     L8878
        ldx     SECADR
        jsr     L8273
        ldy     #$00
        lda     #$24
        ldx     #$F6
        sta     $02AC
        stx     $02AD
        jmp     L98E5

L9C93:  jsr     L9B54
        ldx     #$08
        bcs     L9C9C
        ldx     $61
L9C9C:  stx     DEVNUM
        rts

        jsr     L9D2C
        jsr     L9657
        jsr     L9C93
        cpx     #$01
        beq     L9D03
        lda     #$02
        jsr     SETLFS
        jsr     L9D34
        jsr     L9CE5
        lda     #$01
        sta     SECADR
        ldy     $62
        ldx     $61
        jsr     L946B
        ldx     $02B6
        ldy     $02B7
        bne     L9CCD
        txa
        beq     L9CCF
L9CCD:  dec     SECADR
L9CCF:  lda     #$00
        jsr     LDFE8
        jsr     L95DB
        txa
        bne     L9CDB
        dey
L9CDB:  dex
        jsr     L946B
        jmp     L95DB

        jmp     L880D

L9CE5:  lda     #$60
        jsr     L8878
        bcs     L9D00
        lda     DEVNUM
        jsr     LDF7D
        lda     SECADR
        jsr     LDF83
        jsr     LDF95
        sta     $61
        jsr     LDF95
        sta     $62
L9D00:  jmp     L880D

L9D03:  ldy     #$FF
        lda     $02B6
        ora     $02B7
        beq     L9D0F
        ldy     #$00
L9D0F:  lda     #$02
        jsr     SETLFS
        jsr     L9D34
        lda     #$D5
        ldx     #$FF
        sta     $02AC
        stx     $02AD
        .byte   $AE
        .byte   $B6
L9D23:  .byte   $02
        ldy     $02B7
        lda     #$00
        jmp     L98E5

L9D2C:  jsr     L880D
        lda     #$C0
        jmp     SETMSG

L9D34:  jsr     L9D40
        lda     FREKZP
        ldx     #$DF
        ldy     #$02
        jmp     SETNAM

L9D40:  ldy     $02D3
        sty     $02D2
        ldy     #$00
L9D48:  sty     FREKZP
        jsr     L9B54
        bcs     L9D59
        ldy     FREKZP
        lda     $61
        sta     L02DF,y
        iny
        bne     L9D48
L9D59:  rts

        jsr     L9D2C
        jsr     L9D34
        jmp     L8785

L9D63:  .byte   $04
        jsr     L3054
        ora     $0480
        bcc     L9D6F
        .byte   $22
        .byte   $54
        .byte   $33
L9D6F:  ora     $0480
        bcc     L9D78
        jsr     L3354
        .byte   $0D
L9D78:  .byte   $80
        .byte   $04
        bcc     L9D80
        jsr     L3B54
        .byte   $0D
L9D80:  .byte   $80
        .byte   $04
        bcc     L9D84
L9D84:  .byte   $22
        .byte   $44
        .byte   $33
        ora     $44C8
        brk
        ora     ($22),y
        .byte   $44
        .byte   $33
        ora     $44C8
        lda     #$01
        .byte   $22
        .byte   $44
        .byte   $33
        ora     $0480
        bcc     L9D9D
        .byte   $22
L9D9D:  .byte   $44
        .byte   $33
        ora     $0480
        bcc     L9DCA
        and     ($87),y
        txs
L9DA7:  brk
        and     ($81,x)
        .byte   $82
        brk
        brk
        eor     L914D,y
        .byte   $92
        stx     $4A
        .byte   $85
L9DB4:  sta     $292C,x
        bit     $2823
L9DBA:  bit     BASIC_BUF_LEN
        brk
        cli
        bit     $24
        brk
L9DC1:  .byte   $1C
        txa
        .byte   $1C
        .byte   $23
        eor     $1B8B,x
        lda     ($9D,x)
L9DCA:  txa
        ora     L9D23,x
        .byte   $8B
        ora     a:$A1,x
        and     #$19
        ldx     $A869
        ora     $2423,y
        .byte   $53
        .byte   $1B
        .byte   $23
        bit     $53
        ora     $A1,y
        .byte   $1A
        .byte   $5B
        .byte   $5B
        lda     $69
        bit     $24
        ldx     $A8AE
        lda     a:$29
        .byte   $7C
        brk
        ora     $9C,x
        adc     $A59C
        adc     #$29
        .byte   $53
        sty     $13
        .byte   $34
        ora     ($A5),y
        adc     #$23
        .byte   $A0
L9E01:  cld
        .byte   $62
        .byte   $5A
        pha
        rol     $62
        sty     $88,x
        .byte   $54
        .byte   $44
        iny
        .byte   $54
        pla
        .byte   $44
        inx
        sty     $00,x
        ldy     $08,x
        sty     $74
        ldy     XSIZE,x
        ror     $F474
        cpy     $724A
        .byte   $F2
        ldy     $8A
        brk
        tax
        ldx     #$A2
        .byte   $74
        .byte   $74
        .byte   $74
        .byte   $72
        .byte   $44
        pla
        .byte   $B2
        .byte   $32
        .byte   $B2
        brk
        .byte   $22
        brk
        .byte   $1A
        .byte   $1A
        rol     $26
        .byte   $72
        .byte   $72
        dey
        iny
        cpy     $CA
        rol     $48
        .byte   $44
        .byte   $44
        ldx     #$C8
L9E41:  brk
        .byte   $2F
        lsr     $5752
        .byte   $3E
L9E47:  .byte   $5C
        and     $51
        .byte   $5A
        eor     #$58
        eor     $4840,y
        .byte   $54
        lsr     $53,x
        eor     $474C
        lsr     a
        .byte   $3A
        .byte   $3B
        rol     a
        lsr     $243D,x
        bit     $3C20
        .byte   $2E
        .byte   $5D
L9E62:  .byte   $2F
L9E63:  txs
        .byte   $2F
        txs
        .byte   $0C
        .byte   $9C
        .byte   $9E
        .byte   $9C
        .byte   $3F
        .byte   $9C
        eor     $C49D,y
        sta     $2C,x
        sta     L97D7,y
        sta     $97,x
        eor     ($97),y
        .byte   $54
        .byte   $97
        .byte   $57
        .byte   $97
        adc     $97,x
        eor     #$98
        eor     $0A99,x
        .byte   $97
        ror     $99
        .byte   $47
        stx     $45,y
        .byte   $97
        .byte   $FA
        tya
        .byte   $A7
        tya
        bit     $96
        bit     $96
        jmp     (LF299)

        sta     L99AE,y
        .byte   $BF
        .byte   $87
        .byte   $FF
        .byte   $FF
        brk
        brk
        brk
        brk
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        brk
        brk
        brk
        brk
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        brk
        brk
        brk
        brk
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        brk
        brk
        brk
        brk
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        brk
        brk
        brk
        brk
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        brk
        brk
        brk
        brk
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        brk
        brk
        brk
        brk
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        brk
        brk
        brk
        brk
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        brk
        brk
        brk
        brk
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        brk
        brk
        brk
        brk
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        brk
        brk
        brk
        brk
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        brk
        brk
        brk
        brk
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        brk
        brk
        brk
        brk
sub_DF00_src:
        jsr     sub_DF06
        jmp     L8009

; ???
sub_DF06_src:
        sei
        php
        pha
        lda     PIO
        sta     $A5
        ora     #$03
        sei
        sta     PIO
        bne     pokeIO1a
        cli
; Mask interrupts and poke I/O1 for a while if carry set
pokeIO1_src:
        sei
        php
        pha
pokeIO1a:
        lda     #$0C
:       inc     IO1,x
        inc     IO1,x
        sbc     #$01
        bcs     :-
        pla
        plp
        rts

sub_DF27_src:
        sta     PIO
        lda     (FNAM),y
        stx     PIO
        rts

        jsr     sub_DF06
        jmp     L8120

; Store %10100101 to PIO; banks out ROML/BASIC/KERNAL?
setHIRAM_src:
        lda     $A5
        sta     PIO
; Delay for a while and then enable interrupts
delayedcli_src:
        php
        pha
        lda     #$FF
:       sbc     #$01
        pha
        pla
        bcs     :-
        pla
        plp
        cli
        rts

sub_DF46_src:
        jsr     setHIRAM
        lda     $90
        ldy     $AF
sub_DF4D_rts:
        rts

        jsr     CIOUT
        jmp     pokeIO1

        jsr     LED11
        txa
        jsr     SECOND
        jmp     pokeIO1

        jsr     UNLSN
        jmp     pokeIO1

        jsr     LE453
        jmp     pokeIO1

        jsr     sub_DF06
        jmp     L8703

        jsr     delayedcli
        jmp     LA490

        cli
        jsr     LA560
        jmp     pokeIO1

        jsr     TALK
        jmp     pokeIO1

        jsr     TKSA
        jmp     pokeIO1

        jsr     LE716
        jmp     pokeIO1

        jsr     UNTLK
        jmp     pokeIO1

        jsr     ACPTR
        jmp     pokeIO1

        jsr     LDFA1
        jmp     pokeIO1

        jsr     LF3D5
        rts

        jsr     LBDCD
        jmp     pokeIO1

        jsr     LF5AF
        jmp     pokeIO1

        jsr     LF5D2
        jmp     pokeIO1

        nop
        jsr     LEB48
        lda     KEY_BUF
        cmp     #$83
        bne     sub_DF4D_rts
        lda     CIA1_PRB
        and     #$20
        bne     sub_DF4D_rts
        jsr     sub_DF06
        jmp     L887D

        jsr     sub_DF06
        jmp     L8620

        jsr     sub_DF06
        jmp     L85E0

        cli
        jsr     GETIN
        jmp     pokeIO1

        jsr     PLOT
        jmp     pokeIO1

        jsr     LOAD
        jmp     pokeIO1

        jsr     sub_DF06
        jmp     L8664

        lda     $A5
        jsr     sub_DF06
        jmp     L8675

        .byte   $00,$00,$00,$00
