;   WARNING! This is a modified version of the original asminc/c64.inc
;   taken from <https://github.com/cc65/cc65> commit 4dda5d21.

;
; Olli Savia <ops@iki.fi>
;
; Commodore Kernal functions
;

  CINT         := $FF81
  IOINIT       := $FF84
  RAMTAS       := $FF87

  RESTOR       := $FF8A
  VECTOR       := $FF8D

  SETMSG       := $FF90
  SECOND       := $FF93
  TKSA         := $FF96
  MEMTOP       := $FF99
  MEMBOT       := $FF9C
  SCNKEY       := $FF9F
  SETTMO       := $FFA2
  ACPTR        := $FFA5
  CIOUT        := $FFA8
  UNTLK        := $FFAB
  UNLSN        := $FFAE
  LISTEN       := $FFB1
  TALK         := $FFB4
  READST       := $FFB7
  SETLFS       := $FFBA
  SETNAM       := $FFBD
  OPEN         := $FFC0
  CLOSE        := $FFC3

; Available on all platforms including PET
CHKIN          := $FFC6
CKOUT          := $FFC9
;CHKOUT        := $FFC9
CLRCH          := $FFCC
;CLRCHN        := $FFCC
BASIN          := $FFCF
;CHRIN         := $FFCF
BSOUT          := $FFD2
;CHROUT        := $FFD2

  LOAD         := $FFD5
  SAVE         := $FFD8
  SETTIM       := $FFDB
  RDTIM        := $FFDE

; Available on all platforms including PET
STOP           := $FFE1
GETIN          := $FFE4
CLALL          := $FFE7
UDTIM          := $FFEA

  SCREEN       := $FFED
  PLOT         := $FFF0
  IOBASE       := $FFF3

; ---------------------------------------------------------------------------
; Kernal routines, direct entries
;
; Unlike the above, these are not standard functions with entries in the jump
; table. They do not exist in all Kernals, and where they do the entry point is
; specific to that particular machine and possibly even Kernal version.
;
; This list is not comprehensive: missing items for particular machines
; should be added as needed.
;
; UPDCRAMPTR: Updates the color RAM pointer to match the screen RAM pointer.
;

  CLRSCR       := $E544
  KBDREAD      := $E5B4
  NMIEXIT      := $FEBC
  UPDCRAMPTR   := $EA24
